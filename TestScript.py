#!/usr/bin/env python
import filecmp
import os
import fileinput
import sys

def prRed(skk): print("\033[91m {}\033[00m" .format(skk))
def prGreen(skk): print("\033[92m {}\033[00m" .format(skk))
def prYellow(skk): print("\033[93m {}\033[00m" .format(skk))
def prLightPurple(skk): print("\033[94m {}\033[00m" .format(skk))
def prPurple(skk): print("\033[95m {}\033[00m" .format(skk))
def prCyan(skk): print("\033[96m {}\033[00m" .format(skk))
def prLightGray(skk): print("\033[97m {}\033[00m" .format(skk))
def prBlack(skk): print("\033[98m {}\033[00m" .format(skk))


def replace_words(base_text, device_values):
    for key, val in device_values.items():
        base_text = base_text.replace(key, val)
    return base_text

simulate_3d = False # valid values are "True" and "False"
simulation_style = "pipeline" # valid values are "pipeline_wrapper" and "pipeline"
num_input_points_sim = 460800

# Change the testbench in the Makefile
replace_set = {}
replace_set["           testbench/"] = "            testbench/tempbench.sv #"
t = open('Makefile', 'r')
tempstr = t.read()
t.close()
output = replace_words(tempstr, replace_set)
fout = open('Makefile', 'w')
fout.write(output)
fout.close()

# Comment out any SAIF generation lines
replace_set = {}
replace_set["localparam NUM_INPUT_POINTS_SIM"] = "localparam NUM_INPUT_POINTS_SIM  = %d; //" % num_input_points_sim
replace_set["`define LOOP_PIPELINES"] = "// `define LOOP_PIPELINES"
replace_set["`define LOOP_SLICES"] = "// `define LOOP_SLICES"
t = open('testbench/testbench_%s.sv' % simulation_style, 'r')
tempstr = t.read()
t.close()
output = replace_words(tempstr, replace_set)
fout = open('testbench/tempbench.sv', 'w')
fout.write(output)
fout.close()

os.system('make clean')
os.system('reset')

if simulate_3d:
    data_folder = "output_data_3d"

    # Perform the 3D tests
    prCyan("\rSimulating 3D with %d Input Points" % (num_input_points_sim))
    if simulation_style == "pipeline_wrapper":
        for z_slice_id in range(0, 1):
            sys.stdout.write('\r' + str("Processing Slice %d, All Pipelines..." % (z_slice_id)))
            sys.stdout.flush() # important..don't remember why

            # Set the current pipeline to simulate in the testbench
            replace_set = {}
            replace_set["localparam Z_SLICE_IDX_SIM"] = "localparam Z_SLICE_IDX_SIM        = %d; //" % z_slice_id
            t = open('testbench/tempbench.sv', 'r')
            tempstr = t.read()
            t.close()
            output = replace_words(tempstr, replace_set)
            # Write the updated file
            fout = open('testbench/tempbench.sv', 'w')
            fout.write(output)
            fout.close()

            # Run the simulation
            # os.system('make') # show make output
            os.system('make > /dev/null') # hide make output

            # Copy the output somewhere we can check it
            os.system('cat %s/final_output_zslice%d_ALL.out | tr -d "[:blank:]" > %s/checker_files/test.out.check'  % (data_folder, z_slice_id, data_folder))

            if filecmp.cmp('%s/checker_files/test.out.check' % (data_folder), '%s/checker_files/%s/final_output_zslice%d_ALL.out' % (data_folder, num_input_points_sim, z_slice_id)):
                prGreen("\rAll Pipelines Match!" + ' ' * 20)
            else:
                prRed("\rAll Pipelines Don't Match...Try Running Individual Pipeline Simulations." + ' ' * 20)

            # Reset the testbench file to its previous state
            replace_set = {}
            replace_set["localparam Z_SLICE_IDX_SIM        = %d; //" % z_slice_id] = "localparam Z_SLICE_IDX_SIM"
            t = open('testbench/tempbench.sv', 'r')
            tempstr = t.read()
            t.close()
            output = replace_words(tempstr, replace_set)
            # Write the updated file
            fout = open('testbench/tempbench.sv', 'w')
            fout.write(output)
            fout.close()

        os.remove('%s/checker_files/test.out.check' % (data_folder))
    elif simulation_style == "pipeline":
        for z_slice_id in range(0, 1):
            for pipeline_x_id in range(0, 8):
                for pipeline_y_id in range(0, 8):
                    sys.stdout.write('\r' + str("Processing Slice %d, Pipeline (%d, %d)..." % (z_slice_id, pipeline_x_id, pipeline_y_id)))
                    sys.stdout.flush() # important..don't remember why

                    # Set the current pipeline to simulate in the testbench
                    replace_set = {}
                    replace_set["localparam Z_SLICE_IDX_SIM"] = "localparam Z_SLICE_IDX_SIM        = %d; //" % z_slice_id
                    replace_set["localparam X_ACCEL_IDX_SIM"] = "localparam X_ACCEL_IDX_SIM        = %d; //" % pipeline_x_id
                    replace_set["localparam Y_ACCEL_IDX_SIM"] = "localparam Y_ACCEL_IDX_SIM        = %d; //" % pipeline_y_id
                    t = open('testbench/tempbench.sv', 'r')
                    tempstr = t.read()
                    t.close()
                    output = replace_words(tempstr, replace_set)
                    # Write the updated file
                    fout = open('testbench/tempbench.sv', 'w')
                    fout.write(output)
                    fout.close()

                    # Run the simulation
                    # os.system('make') # show make output
                    os.system('make > /dev/null') # hide make output

                    # Copy the output somewhere we can check it
                    os.system('cat %s/final_output_zslice%d_x%d_y%d.out | tr -d "[:blank:]" > %s/checker_files/test.out.check' % (data_folder, z_slice_id, pipeline_x_id, pipeline_y_id, data_folder))

                    if filecmp.cmp('%s/checker_files/test.out.check' % (data_folder), '%s/checker_files/%s/final_output_zslice%d_x%d_y%d.out' % (data_folder, num_input_points_sim, z_slice_id, pipeline_x_id, pipeline_y_id)):
                        prGreen("\rSlice %d, Pipeline (%d, %d)" % (z_slice_id, pipeline_x_id, pipeline_y_id) + ' ' * 20)
                    else:
                        prRed("\rSlice %d, Pipeline (%d, %d)" % (z_slice_id, pipeline_x_id, pipeline_y_id) + ' ' * 20)

                    # Reset the testbench file to its previous state
                    replace_set = {}
                    replace_set["localparam Z_SLICE_IDX_SIM        = %d; //" % z_slice_id] = "localparam Z_SLICE_IDX_SIM"
                    replace_set["localparam X_ACCEL_IDX_SIM        = %d; //" % pipeline_x_id] = "localparam X_ACCEL_IDX_SIM"
                    replace_set["localparam Y_ACCEL_IDX_SIM        = %d; //" % pipeline_y_id] = "localparam Y_ACCEL_IDX_SIM"
                    t = open('testbench/tempbench.sv', 'r')
                    tempstr = t.read()
                    t.close()
                    output = replace_words(tempstr, replace_set)
                    # Write the updated file
                    fout = open('testbench/tempbench.sv', 'w')
                    fout.write(output)
                    fout.close()

        os.remove('%s/checker_files/test.out.check' % (data_folder))
    else:
        prRed("\rERROR: %s is not a known/supported simulation type!" % (simulation_style) + ' ' * 20)
else:
    data_folder = "output_data"

    # IF ENABLED, disable 3D hardware in the sys_defs file
    replace_set = {}
    replace_set["`define USE_3D"] = "//`define USE_3D"
    t = open('src/sys_defs.vh', 'r')
    tempstr = t.read()
    t.close()
    output = replace_words(tempstr, replace_set)
    fout = open('src/sys_defs.vh', 'w')
    fout.write(output)
    fout.close()

    # Peform the 2D tests
    prCyan("\rSimulating 2D with %d Input Points" % (num_input_points_sim))
    if simulation_style == "pipeline_wrapper":
        sys.stdout.write('\r' + str("Processing All Pipelines..."))
        sys.stdout.flush() # important..don't remember why

        # Run the simulation
        # os.system('make') # show make output
        os.system('make > /dev/null') # hide make output

        # Copy the output somewhere we can check it
        os.system('cat %s/final_output_ALL.out | tr -d "[:blank:]" > %s/checker_files/test.out.check'  % (data_folder, data_folder))

        if filecmp.cmp('%s/checker_files/test.out.check' % (data_folder), '%s/checker_files/%s/final_output_ALL.out' % (data_folder, num_input_points_sim)):
            prGreen("\rAll Pipelines Match!" + ' ' * 20)
            # prGreen("Pass :D")
        else:
            prRed("\rAll Pipelines Don't Match...Try Running Individual Pipeline Simulations." + ' ' * 20)
            # prRed("Fail :(")

        os.remove('%s/checker_files/test.out.check' % (data_folder))
    elif simulation_style == "pipeline":
        for pipeline_x_id in range(0, 8):
            for pipeline_y_id in range(0, 8):
                sys.stdout.write('\r' + str("Processing Pipeline (%d, %d)..." % (pipeline_x_id, pipeline_y_id)))
                sys.stdout.flush() # important..don't remember why

                # Set the current pipeline to simulate in the testbench
                replace_set = {}
                replace_set["localparam X_ACCEL_IDX_SIM"] = "localparam X_ACCEL_IDX_SIM        = %d; //" % pipeline_x_id
                replace_set["localparam Y_ACCEL_IDX_SIM"] = "localparam Y_ACCEL_IDX_SIM        = %d; //" % pipeline_y_id
                t = open('testbench/tempbench.sv', 'r')
                tempstr = t.read()
                t.close()
                output = replace_words(tempstr, replace_set)
                # Write the updated file
                fout = open('testbench/tempbench.sv', 'w')
                fout.write(output)
                fout.close()

                # Run the simulation
                # os.system('make') # show make output
                os.system('make > /dev/null') # hide make output

                # Copy the output somewhere we can check it
                os.system('cat %s/final_output_x%d_y%d.out | tr -d "[:blank:]" > %s/checker_files/test.out.check' % (data_folder, pipeline_x_id, pipeline_y_id, data_folder))

                if filecmp.cmp('%s/checker_files/test.out.check' % (data_folder), '%s/checker_files/%s/final_output_x%d_y%d.out' % (data_folder, num_input_points_sim, pipeline_x_id, pipeline_y_id)):
                    prGreen("\rPipeline (%d, %d)" % (pipeline_x_id, pipeline_y_id) + ' ' * 20)
                    # prGreen("Pass :D")
                else:
                    prRed("\rPipeline (%d, %d)" % (pipeline_x_id, pipeline_y_id) + ' ' * 20)
                    # prRed("Fail :(")

                # Reset the testbench file to its previous state
                replace_set = {}
                replace_set["localparam X_ACCEL_IDX_SIM        = %d; //" % pipeline_x_id] = "localparam X_ACCEL_IDX_SIM"
                replace_set["localparam Y_ACCEL_IDX_SIM        = %d; //" % pipeline_y_id] = "localparam Y_ACCEL_IDX_SIM"
                t = open('testbench/tempbench.sv', 'r')
                tempstr = t.read()
                t.close()
                output = replace_words(tempstr, replace_set)
                # Write the updated file
                fout = open('testbench/tempbench.sv', 'w')
                fout.write(output)
                fout.close()

        os.remove('%s/checker_files/test.out.check' % (data_folder))
    else:
        prRed("\rERROR: %s is not a known/supported simulation type!" % (simulation_style) + ' ' * 20)

    # Reset 3D hardware support in the sys_defs file
    replace_set = {}
    replace_set["//`define USE_3D"] = "`define USE_3D"
    t = open('src/sys_defs.vh', 'r')
    tempstr = t.read()
    t.close()
    output = replace_words(tempstr, replace_set)
    fout = open('src/sys_defs.vh', 'w')
    fout.write(output)
    fout.close()

# Reset the testbench in the Makefile
replace_set = {}
replace_set["           testbench/tempbench.sv #"] = "          testbench/"
t = open('Makefile', 'r')
tempstr = t.read()
t.close()
output = replace_words(tempstr, replace_set)
fout = open('Makefile', 'w')
fout.write(output)
fout.close()
