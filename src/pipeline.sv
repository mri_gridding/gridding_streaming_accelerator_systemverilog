/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  pipeline.sv                                         //
//                                                                     //
//  Description :  Top-level module of the interpolation pipeline;     //
//                 this instantiates and connects the components of    //
//                 the interpolation pipeline togeather.               //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module pipeline #(
    `ifdef USE_3D
    parameter MAX_GRID_DIM_Z          = 1024,
    `ifdef USE_3D_CUBE
    parameter ACCEL_DIM_Z             = 8,
    parameter TILE_DIM_Z              = (MAX_GRID_DIM_Z/ACCEL_DIM_Z),
    `endif
    `endif
    parameter ACCEL_DIM_X             = 8,
    parameter ACCEL_DIM_Y             = 8,
    parameter MAX_GRID_DIM_X          = 1024,
    parameter MAX_GRID_DIM_Y          = 1024,
    parameter TILE_DIM_X              = (MAX_GRID_DIM_X/ACCEL_DIM_X),
    parameter TILE_DIM_Y              = (MAX_GRID_DIM_Y/ACCEL_DIM_Y),
    parameter MAX_INTERP_WINDOW_SIZE  = ACCEL_DIM_X,
    parameter SAMPLE_BIT_WIDTH        = 32,
    parameter SAMPLE_COORD_BIT_WIDTH  = 32,
    parameter TABLE_BIT_WIDTH         = 16,
    parameter TABLE_FRAC_WIDTH        = 14,
    parameter TABLE_OVERSAMP          = 32,
    parameter NUM_TABLE_ENTRIES       = MAX_INTERP_WINDOW_SIZE*TABLE_OVERSAMP,
    `ifdef USE_3D_CUBE
    parameter NUM_BANKS               = 16,
    parameter NUM_BANK_ENTRIES        = 131072,
    `else
    parameter NUM_BANKS               = 4,
    parameter NUM_BANK_ENTRIES        = 4096,
    `endif
    parameter NUM_BUFFER_ENTRIES      = NUM_BANKS*NUM_BANK_ENTRIES
    ) (

        // Inputs
        `ifdef USE_3D
        input [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_z_coord_in,
        `ifdef USE_3D_CUBE
        input [$clog2(ACCEL_DIM_Z)-1:0]            accel_z_idx_in,
        input [$clog2(TILE_DIM_Z)-1:0]             tile_z_dim_in,
        `else
        input [$clog2(MAX_GRID_DIM_Z)-1:0]         z_slice_idx_in,
        `endif
        `endif
        input                                      clock,
        input                                      reset,
        input                                      init_mem,
        input [$clog2(ACCEL_DIM_X)-1:0]            accel_x_idx_in,
        input [$clog2(ACCEL_DIM_Y)-1:0]            accel_y_idx_in,
        input [$clog2(TILE_DIM_X)-1:0]             tile_x_dim_in,
        input [$clog2(TILE_DIM_Y)-1:0]             tile_y_dim_in,
        input [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0] window_dim_in,
        input [(TABLE_BIT_WIDTH*2)-1:0]            interp_table_consts_in,
        input [$clog2(NUM_TABLE_ENTRIES)-1:0]      interp_table_consts_addr_in,
        input                                      interp_table_consts_en_in,
        input [$clog2(NUM_BUFFER_ENTRIES)-1:0]     rd_out_data_addr_in, // read/write tile address (can be used for reading final values out)
        input                                      rd_out_data_en_in, // output a value (for reading final values out)
        input [(SAMPLE_BIT_WIDTH*2)-1:0]           sample_data_in,
        input [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_x_coord_in,
        input [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_y_coord_in,
        input                                      sample_en_in,

        // Outputs
        output logic [(SAMPLE_BIT_WIDTH*2)-1:0] data_out,
        output logic                            data_en_out

    );


    // Signals

    // Select -> Interp Table Signals
    `ifdef USE_3D
    logic [$clog2(NUM_TABLE_ENTRIES)-1:0] select_z_table_idx_out;
    `endif
    logic [$clog2(NUM_TABLE_ENTRIES)-1:0] select_x_table_idx_out;
    logic [$clog2(NUM_TABLE_ENTRIES)-1:0] select_y_table_idx_out;

    // Select -> Interp Signals
    logic [(SAMPLE_BIT_WIDTH*2)-1:0] select_data_out, select_data_out_2, select_data_out_3, select_data_out_4, select_data_out_5;
    logic                            select_en_out, select_en_out_2, select_en_out_3, select_en_out_4, select_en_out_5;

    // Select -> Accum Signals
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] select_tile_idx_out, tile_idx_out_2, tile_idx_out_3, tile_idx_out_4, tile_idx_out_5, tile_idx_out_6, tile_idx_out_7, tile_idx_out_8, tile_idx_out_9, tile_idx_out_10, tile_idx_out_11, tile_idx_out_12;

    // Interp Table -> Interp Signals
    logic [TABLE_BIT_WIDTH-1:0]            interp_table_real_out;
    logic [TABLE_BIT_WIDTH-1:0]            interp_table_imag_out;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] interp_table_tile_idx_out;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       interp_table_data_out;
    logic                                  interp_table_en_out;

    // Interp -> Accum Signals
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] interp_tile_idx_out;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       interp_data_out;
    logic                                  interp_en_out;


    // Select Module
    select #(
        `ifdef USE_3D
        .MAX_GRID_DIM_Z(MAX_GRID_DIM_Z),
        `ifdef USE_3D_CUBE
        .ACCEL_DIM_Z(ACCEL_DIM_Z),
        .TILE_DIM_Z(TILE_DIM_Z),
        `endif
        `endif
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .SAMPLE_COORD_BIT_WIDTH(SAMPLE_COORD_BIT_WIDTH),
        .TABLE_OVERSAMP(TABLE_OVERSAMP),
        .ACCEL_DIM_X(ACCEL_DIM_X),
        .ACCEL_DIM_Y(ACCEL_DIM_Y),
        .MAX_GRID_DIM_X(MAX_GRID_DIM_X),
        .MAX_GRID_DIM_Y(MAX_GRID_DIM_Y),
        .TILE_DIM_X(TILE_DIM_X),
        .TILE_DIM_Y(TILE_DIM_Y),
        .MAX_INTERP_WINDOW_SIZE(MAX_INTERP_WINDOW_SIZE),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES)
        ) slct (

        // Inputs
        `ifdef USE_3D
        .sample_z_coord_in(sample_z_coord_in),
        `ifdef USE_3D_CUBE
        .accel_z_idx_in(accel_z_idx_in),
        .tile_z_dim_in(tile_z_dim_in),
        `else
        .z_slice_idx_in(z_slice_idx_in),
        `endif
        `endif
        .clock(clock),
        .reset(reset),
        .sample_data_in(sample_data_in),
        .sample_x_coord_in(sample_x_coord_in),
        .sample_y_coord_in(sample_y_coord_in),
        .sample_en_in(sample_en_in),
        .accel_x_idx_in(accel_x_idx_in),
        .accel_y_idx_in(accel_y_idx_in),
        .tile_x_dim_in(tile_x_dim_in),
        .tile_y_dim_in(tile_y_dim_in),
        .window_dim_in(window_dim_in),

        // Outputs
        `ifdef USE_3D
        .select_z_table_idx_out(select_z_table_idx_out),
        `endif
        .select_x_table_idx_out(select_x_table_idx_out),
        .select_y_table_idx_out(select_y_table_idx_out),
        .select_tile_idx_out(select_tile_idx_out),
        .select_data_out(select_data_out),
        .select_en_out(select_en_out)

    );


    // Interpolation Table (Oversampling)
    interp_table #(
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .TABLE_BIT_WIDTH(TABLE_BIT_WIDTH),
        .MAX_INTERP_WINDOW_SIZE(MAX_INTERP_WINDOW_SIZE),
        .TABLE_OVERSAMP(TABLE_OVERSAMP),
        .NUM_TABLE_ENTRIES(NUM_TABLE_ENTRIES),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES)
        ) ntrp_tbl (

        // Inputs
        `ifdef USE_3D
        .select_addr_z_in(select_z_table_idx_out),
        `endif
        .clock(clock),
        .reset(reset),
        .init_mem(init_mem),
        .select_addr_x_in(select_x_table_idx_out),
        .select_addr_y_in(select_y_table_idx_out),
        .select_tile_idx_in(select_tile_idx_out),
        .select_data_in(select_data_out),
        .select_en_in(select_en_out),
        .interp_table_consts_in(interp_table_consts_in),
        .interp_table_consts_addr_in(interp_table_consts_addr_in),
        .interp_table_consts_en_in(interp_table_consts_en_in),

        // Outputs
        .interp_table_real_out(interp_table_real_out),
        .interp_table_imag_out(interp_table_imag_out),
        .interp_table_tile_idx_out(interp_table_tile_idx_out),
        .interp_table_data_out(interp_table_data_out),
        .interp_table_en_out(interp_table_en_out)

    );


    // Interpolation Module
    interp #(
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .TABLE_BIT_WIDTH(TABLE_BIT_WIDTH),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES)
        ) ntrp (

        // Inputs
        .clock(clock),
        .reset(reset),
        .interp_table_tile_idx_in(interp_table_tile_idx_out),
        .interp_table_data_in(interp_table_data_out),
        .interp_table_const_real_in(interp_table_real_out),
        .interp_table_const_imag_in(interp_table_imag_out),
        .interp_table_en_in(interp_table_en_out),

        // Outputs
        .interp_tile_idx_out(interp_tile_idx_out),
        .interp_data_out(interp_data_out),
        .interp_en_out(interp_en_out)

    );


    // Accumulate Module
    accum #(
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .NUM_BANKS(NUM_BANKS),
        .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES)
        ) ccm (

        // Inputs
        .clock(clock),
        .reset(reset),
        .init_mem(init_mem),
        .interp_tile_idx_in(rd_out_data_en_in ? rd_out_data_addr_in : interp_tile_idx_out),
        .interp_data_in(interp_data_out),
        .interp_en_in(interp_en_out),
        .accum_rd_en_in(rd_out_data_en_in),

        // Outputs
        .accum_data_out(data_out),
        .accum_en_out(data_en_out)

    );

endmodule // pipeline
