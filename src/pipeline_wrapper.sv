/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  pipeline_wrapper.sv                                 //
//                                                                     //
//  Description :  This instantiates and connects the individual       //
//                 interpolation pipelines.                            //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module pipeline_wrapper #(
    `ifdef USE_3D
    parameter MAX_GRID_DIM_Z          = 1024,
    `ifdef USE_3D_CUBE
    parameter ACCEL_DIM_Z             = 8,
    parameter TILE_DIM_Z              = (MAX_GRID_DIM_Z/ACCEL_DIM_Z),
    `endif
    `endif
    parameter ACCEL_DIM_X             = 8,
    parameter ACCEL_DIM_Y             = 8,
    parameter MAX_GRID_DIM_X          = 1024,
    parameter MAX_GRID_DIM_Y          = 1024,
    parameter TILE_DIM_X              = (MAX_GRID_DIM_X/ACCEL_DIM_X),
    parameter TILE_DIM_Y              = (MAX_GRID_DIM_Y/ACCEL_DIM_Y),
    parameter MAX_INTERP_WINDOW_SIZE  = ACCEL_DIM_X,
    parameter SAMPLE_BIT_WIDTH        = 32,
    parameter SAMPLE_COORD_BIT_WIDTH  = 32,
    parameter TABLE_BIT_WIDTH         = 16,
    parameter TABLE_FRAC_WIDTH        = 14,
    parameter TABLE_OVERSAMP          = 32,
    parameter NUM_TABLE_ENTRIES       = MAX_INTERP_WINDOW_SIZE*TABLE_OVERSAMP,
    `ifdef USE_3D_CUBE
    parameter NUM_BANKS               = 16,
    parameter NUM_BANK_ENTRIES        = 131072,
    `else
    parameter NUM_BANKS               = 4,
    parameter NUM_BANK_ENTRIES        = 4096,
    `endif
    parameter NUM_BUFFER_ENTRIES      = NUM_BANKS*NUM_BANK_ENTRIES
    ) (

        // Inputs
        `ifdef USE_3D
        input [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_z_coord_in,
        `ifdef USE_3D_CUBE
        input [$clog2(TILE_DIM_Z)-1:0]             tile_z_dim_in,
        `else
        input [$clog2(MAX_GRID_DIM_Z)-1:0]         z_slice_idx_in,
        `endif
        `endif
        input                                      clock,
        input                                      reset,
        input                                      init_mem,
        input [$clog2(TILE_DIM_X)-1:0]             tile_x_dim_in,
        input [$clog2(TILE_DIM_Y)-1:0]             tile_y_dim_in,
        input [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0] window_dim_in,
        input [(TABLE_BIT_WIDTH*2)-1:0]            interp_table_consts_in,
        input [$clog2(NUM_TABLE_ENTRIES)-1:0]      interp_table_consts_addr_in,
        input                                      interp_table_consts_en_in,
        input [$clog2(NUM_BUFFER_ENTRIES)-1:0]     rd_out_data_addr_in, // read/write tile address (can be used for reading final values out)
        input                                      rd_out_data_en_in, // output a value (for reading final values out)
        input [(SAMPLE_BIT_WIDTH*2)-1:0]           sample_data_in,
        input [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_x_coord_in,
        input [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_y_coord_in,
        input                                      sample_en_in,

        // Outputs
        `ifdef USE_3D_CUBE
        output logic [ACCEL_DIM_X-1:0][ACCEL_DIM_Y-1:0][ACCEL_DIM_Z-1:0][(SAMPLE_BIT_WIDTH*2)-1:0] data_out,
        output logic [ACCEL_DIM_X-1:0][ACCEL_DIM_Y-1:0][ACCEL_DIM_Z-1:0]                           data_en_out
        `else
        output logic [ACCEL_DIM_X-1:0][ACCEL_DIM_Y-1:0][(SAMPLE_BIT_WIDTH*2)-1:0] data_out,
        output logic [ACCEL_DIM_X-1:0][ACCEL_DIM_Y-1:0]                           data_en_out
        `endif

    );

    // Generate the pipelines (make one slice)
    genvar gen_x_idx, gen_y_idx;
    `ifdef USE_3D_CUBE
    genvar gen_z_idx;
    `endif
    generate
        for(gen_x_idx = 0; gen_x_idx < ACCEL_DIM_X; gen_x_idx++) begin : gen_pipelines_x
            for(gen_y_idx = 0; gen_y_idx < ACCEL_DIM_Y; gen_y_idx++) begin : gen_pipelines_y
                `ifdef USE_3D_CUBE
                    for(gen_z_idx = 0; gen_z_idx < ACCEL_DIM_Z; gen_z_idx++) begin : gen_pipelines_z
                `endif
                    // Instantiate the pipelines
                    pipeline #(
                        `ifdef USE_3D
                        .MAX_GRID_DIM_Z(MAX_GRID_DIM_Z),
                        `ifdef USE_3D_CUBE
                        .ACCEL_DIM_Z(ACCEL_DIM_Z),
                        .TILE_DIM_Z(TILE_DIM_Z),
                        `endif
                        `endif
                        .ACCEL_DIM_X(ACCEL_DIM_X),
                        .ACCEL_DIM_Y(ACCEL_DIM_Y),
                        .MAX_GRID_DIM_X(MAX_GRID_DIM_X),
                        .MAX_GRID_DIM_Y(MAX_GRID_DIM_Y),
                        .TILE_DIM_X(TILE_DIM_X),
                        .TILE_DIM_Y(TILE_DIM_Y),
                        .MAX_INTERP_WINDOW_SIZE(MAX_INTERP_WINDOW_SIZE),
                        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
                        .SAMPLE_COORD_BIT_WIDTH(SAMPLE_COORD_BIT_WIDTH),
                        .TABLE_BIT_WIDTH(TABLE_BIT_WIDTH),
                        .TABLE_OVERSAMP(TABLE_OVERSAMP),
                        .NUM_TABLE_ENTRIES(NUM_TABLE_ENTRIES),
                        .NUM_BANKS(NUM_BANKS),
                        .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES),
                        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES)
                        ) pipeline_0 (

                        // Inputs
                        `ifdef USE_3D
                        .sample_z_coord_in(sample_z_coord_in),
                        `ifdef USE_3D_CUBE
                        .accel_z_idx_in(gen_z_idx[$clog2(ACCEL_DIM_Z)-1:0]),
                        .tile_z_dim_in(tile_z_dim_in),
                        `else
                        .z_slice_idx_in(z_slice_idx_in),
                        `endif
                        `endif
                        .clock(clock),
                        .reset(reset),
                        .init_mem(init_mem),
                        .accel_x_idx_in(gen_x_idx[$clog2(ACCEL_DIM_X)-1:0]),
                        .accel_y_idx_in(gen_y_idx[$clog2(ACCEL_DIM_Y)-1:0]),
                        .tile_x_dim_in(tile_x_dim_in),
                        .tile_y_dim_in(tile_y_dim_in),
                        .window_dim_in(window_dim_in),
                        .interp_table_consts_in(interp_table_consts_in),
                        .interp_table_consts_addr_in(interp_table_consts_addr_in),
                        .interp_table_consts_en_in(interp_table_consts_en_in),
                        .rd_out_data_addr_in(rd_out_data_addr_in),
                        .rd_out_data_en_in(rd_out_data_en_in),
                        .sample_data_in(sample_data_in),
                        .sample_x_coord_in(sample_x_coord_in),
                        .sample_y_coord_in(sample_y_coord_in),
                        .sample_en_in(sample_en_in),

                        // Outputs
                        `ifdef USE_3D_CUBE
                        .data_out(data_out[gen_x_idx][gen_y_idx][gen_z_idx]),
                        .data_en_out(data_en_out[gen_x_idx][gen_y_idx][gen_z_idx])
                        `else
                        .data_out(data_out[gen_x_idx][gen_y_idx]),
                        .data_en_out(data_en_out[gen_x_idx][gen_y_idx])
                        `endif

                    );
                `ifdef USE_3D_CUBE
                end
                `endif
            end
        end
    endgenerate

endmodule // pipeline_wrapper
