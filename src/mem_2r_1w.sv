/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  mem_2r_1w.sv                                        //
//                                                                     //
//  Description :  This module creates the regfile for the interp      //
//                 table oversamp values.                              //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module mem_2r_1w #(
    parameter ENTRY_BIT_WIDTH = 32,
    parameter NUM_ENTRIES     = 256
    ) (

        // Inputs
        input                           clock,
        input                           init,
        input [$clog2(NUM_ENTRIES)-1:0] rda_idx, // read A index
        input [$clog2(NUM_ENTRIES)-1:0] rdb_idx, // read B index
        input [$clog2(NUM_ENTRIES)-1:0] wr_idx,  // write index
        input [ENTRY_BIT_WIDTH-1:0]     wr_data, // write data
        input                           wr_en,

        // Outputs
        output logic [ENTRY_BIT_WIDTH-1:0] rda_out, // read A data
        output logic [ENTRY_BIT_WIDTH-1:0] rdb_out  // read B data

    );

    logic [NUM_ENTRIES-1:0][ENTRY_BIT_WIDTH-1:0] registers; // registers

    wire [ENTRY_BIT_WIDTH-1:0] rda_reg = registers[rda_idx];
    wire [ENTRY_BIT_WIDTH-1:0] rdb_reg = registers[rdb_idx];

    //
    // Read port A
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock)
        // if(wr_en && (wr_idx == rda_idx))
        //     rda_out <= `SD wr_data; // internal forwarding
        // else
            rda_out <= `SD rda_reg;

    //
    // Read port B
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock)
        // if(wr_en && (wr_idx == rdb_idx))
        //     rdb_out <= `SD wr_data; // internal forwarding
        // else
            rdb_out <= `SD rdb_reg;

    //
    // Write port
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock)
        if(init)
            registers <= `SD '0;
        else
            if(wr_en)
                registers[wr_idx] <= `SD wr_data;

endmodule // mem_2r_1w
