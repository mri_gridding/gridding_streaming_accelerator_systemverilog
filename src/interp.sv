/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  interp.sv                                           //
//                                                                     //
//  Description :  Apodizes selected samples.                          //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module interp #(
    parameter SAMPLE_BIT_WIDTH       = 32,
    parameter SAMPLE_ORIG_FRAC_WIDTH = 40,
    parameter SAMPLE_K9_FRAC_WIDTH   = 30,
    parameter SAMPLE_K12_FRAC_WIDTH  = 36,
    parameter TABLE_BIT_WIDTH        = 16,
    parameter TABLE_FRAC_WIDTH       = 14,
    parameter NUM_BUFFER_ENTRIES     = 256
    ) (

        // Inputs
        input                                  clock,
        input                                  reset,
        input [$clog2(NUM_BUFFER_ENTRIES)-1:0] interp_table_tile_idx_in,
        input [(SAMPLE_BIT_WIDTH*2)-1:0]       interp_table_data_in,
        input [TABLE_BIT_WIDTH-1:0]            interp_table_const_real_in,
        input [TABLE_BIT_WIDTH-1:0]            interp_table_const_imag_in,
        input                                  interp_table_en_in,

        // Outputs
        output logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] interp_tile_idx_out,
        output logic [(SAMPLE_BIT_WIDTH*2)-1:0]       interp_data_out,
        output logic                                  interp_en_out

    );


    // Local variables
    logic [TABLE_BIT_WIDTH-1:0]                                                                    k7, k7_next; // 
    logic [SAMPLE_BIT_WIDTH-1:0]                                                                   k8, k8_next; // 
    logic [SAMPLE_BIT_WIDTH-1:0]                                                                   k9, k9_next; // 
    logic [SAMPLE_BIT_WIDTH-1:0]                                                                   k10, k10_next; // 
    logic [SAMPLE_BIT_WIDTH-1:0]                                                                   k11, k11_next; // 
    logic [SAMPLE_BIT_WIDTH-1:0]                                                                   k12, k12_next; // 
    logic [SAMPLE_BIT_WIDTH-1:0]                                                                   r2_next; // 
    logic [SAMPLE_BIT_WIDTH-1:0]                                                                   i2_next; // 
    logic [SAMPLE_BIT_WIDTH+SAMPLE_ORIG_FRAC_WIDTH+SAMPLE_K9_FRAC_WIDTH-SAMPLE_K12_FRAC_WIDTH-1:0] k10_temp;
    logic [SAMPLE_BIT_WIDTH+SAMPLE_ORIG_FRAC_WIDTH-SAMPLE_BIT_WIDTH-1:0]                           k11_temp, k12_temp;

    logic k9_val_en, k12_val_en, k12_val_en_next, i2_val_en_next;

    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] tile_idx, tile_idx_2, tile_idx_2_next, tile_idx_3_next;

    logic [SAMPLE_BIT_WIDTH-1:0] sample_real, sample_real_next, sample_imag_next;
    logic [TABLE_BIT_WIDTH-1:0]  interp_table_const_real;
    logic [TABLE_BIT_WIDTH-1:0]  interp_table_const_imag;


    // Combine the x and y table values to create the final interpolation constant
    always_comb begin
        k12_val_en_next = k9_val_en;
        i2_val_en_next  = k12_val_en;
        tile_idx_2_next = tile_idx;
        tile_idx_3_next = tile_idx_2;

        sample_real_next = interp_table_data_in[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH];
        sample_imag_next = interp_table_data_in[SAMPLE_BIT_WIDTH-1:0];

        // Cycle 1
        k7_next = $signed(interp_table_const_real_in) - $signed(interp_table_const_imag_in); // table_const_real - table_const_imag
        k8_next = ($signed(sample_imag_next) - $signed(sample_real_next)) >>> (SAMPLE_ORIG_FRAC_WIDTH-SAMPLE_K9_FRAC_WIDTH); // sample_imag - sample_real
        k9_next = ($signed(sample_real_next) + $signed(sample_imag_next)) >>> (SAMPLE_ORIG_FRAC_WIDTH-SAMPLE_K9_FRAC_WIDTH); // sample_real + sample_imag

        // Cycle 2
        k10_temp = $signed(sample_real) * $signed({k7, {(SAMPLE_BIT_WIDTH-TABLE_BIT_WIDTH){1'b0}}}); // sample_real * k7
        k10_next = k10_temp[SAMPLE_BIT_WIDTH+SAMPLE_ORIG_FRAC_WIDTH+SAMPLE_K9_FRAC_WIDTH-SAMPLE_K12_FRAC_WIDTH-1:SAMPLE_ORIG_FRAC_WIDTH+SAMPLE_K9_FRAC_WIDTH-SAMPLE_K12_FRAC_WIDTH]; // sample_real * k7
        k11_temp = $signed(interp_table_const_real) * $signed(k8); // table_const_real * k8
        k11_next = k11_temp[SAMPLE_BIT_WIDTH+SAMPLE_ORIG_FRAC_WIDTH-SAMPLE_BIT_WIDTH-1:SAMPLE_ORIG_FRAC_WIDTH-SAMPLE_BIT_WIDTH]; // table_const_real * k8
        k12_temp = $signed(interp_table_const_imag) * $signed(k9); // table_const_imag * k9
        k12_next = k12_temp[SAMPLE_BIT_WIDTH+SAMPLE_ORIG_FRAC_WIDTH-SAMPLE_BIT_WIDTH-1:SAMPLE_ORIG_FRAC_WIDTH-SAMPLE_BIT_WIDTH]; // table_const_imag * k9

        // Cycle 3
        r2_next = $signed(k10) + $signed(k12); // k10 + k12
        i2_next = $signed(k10) + $signed(k11); // k10 + k11
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            sample_real             <= `SD 0;
            interp_table_const_real <= `SD 0;
            interp_table_const_imag <= `SD 0;

            k9_val_en  <= `SD 0;
            k12_val_en <= `SD 0;
            tile_idx   <= `SD 0;
            tile_idx_2 <= `SD 0;

            k7  <= `SD 0;
            k8  <= `SD 0;
            k9  <= `SD 0;
            k10 <= `SD 0;
            k11 <= `SD 0;
            k12 <= `SD 0;

            interp_tile_idx_out                                      <= `SD 0;
            interp_data_out[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH] <= `SD 0;
            interp_data_out[SAMPLE_BIT_WIDTH-1:0]                    <= `SD 0;
            interp_en_out                                            <= `SD 0;
        end else begin
            sample_real             <= `SD sample_real_next;
            interp_table_const_real <= `SD interp_table_const_real_in;
            interp_table_const_imag <= `SD interp_table_const_imag_in;

            k9_val_en  <= `SD interp_table_en_in;
            k12_val_en <= `SD k12_val_en_next;
            tile_idx   <= `SD interp_table_tile_idx_in;
            tile_idx_2 <= `SD tile_idx_2_next;

            k7  <= `SD k7_next;
            k8  <= `SD k8_next;
            k9  <= `SD k9_next;
            k10 <= `SD k10_next;
            k11 <= `SD k11_next;
            k12 <= `SD k12_next;

            interp_tile_idx_out                                      <= `SD i2_val_en_next ? tile_idx_3_next : '0;
            interp_data_out[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH] <= `SD i2_val_en_next ? r2_next : '0;
            interp_data_out[SAMPLE_BIT_WIDTH-1:0]                    <= `SD i2_val_en_next ? i2_next : '0;
            interp_en_out                                            <= `SD i2_val_en_next;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // interp
