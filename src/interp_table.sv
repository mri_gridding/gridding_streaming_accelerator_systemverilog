/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  interp_table.sv                                     //
//                                                                     //
//  Description :  Apodizes selected samples.                          //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module interp_table #(
    parameter SAMPLE_BIT_WIDTH       = 32,
    parameter TABLE_BIT_WIDTH        = 16,
    parameter TABLE_FRAC_WIDTH       = 14,
    parameter MAX_INTERP_WINDOW_SIZE = 8,
    parameter TABLE_OVERSAMP         = 32,
    parameter NUM_TABLE_ENTRIES      = MAX_INTERP_WINDOW_SIZE*TABLE_OVERSAMP,
    parameter NUM_BUFFER_ENTRIES     = 256
    ) (

        // Inputs
        `ifdef USE_3D
        input [$clog2(NUM_TABLE_ENTRIES)-1:0]  select_addr_z_in,
        `endif
        input                                  clock,
        input                                  reset,
        input                                  init_mem,
        input [$clog2(NUM_TABLE_ENTRIES)-1:0]  select_addr_x_in,
        input [$clog2(NUM_TABLE_ENTRIES)-1:0]  select_addr_y_in,
        input [$clog2(NUM_BUFFER_ENTRIES)-1:0] select_tile_idx_in,
        input [(SAMPLE_BIT_WIDTH*2)-1:0]       select_data_in,
        input                                  select_en_in,
        input [(TABLE_BIT_WIDTH*2)-1:0]        interp_table_consts_in,
        input [$clog2(NUM_TABLE_ENTRIES)-1:0]  interp_table_consts_addr_in,
        input                                  interp_table_consts_en_in,

        // Outputs
        output logic [TABLE_BIT_WIDTH-1:0]            interp_table_real_out,
        output logic [TABLE_BIT_WIDTH-1:0]            interp_table_imag_out,
        output logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] interp_table_tile_idx_out,
        output logic [(SAMPLE_BIT_WIDTH*2)-1:0]       interp_table_data_out,
        output logic                                  interp_table_en_out

    );


    // Local variables
    `ifdef USE_3D
    logic [$clog2(NUM_TABLE_ENTRIES)-1:0]        select_addr_z, select_addr_z_2, select_addr_z_2_next, select_addr_z_3, select_addr_z_3_next;
    logic [(TABLE_BIT_WIDTH*2)-1:0]              rd_z_data_next; // z table value
    logic [TABLE_BIT_WIDTH-1:0]                  table_z_real, table_z_real_next, table_z_imag_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  k7, k7_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  k8, k8_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  k9, k9_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  k10, k10_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  k11, k11_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  k12, k12_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  r2_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  i2_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  r1_saved, r1_saved_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  i1_saved, i1_saved_next; // 
    logic [TABLE_BIT_WIDTH+TABLE_FRAC_WIDTH-1:0] k10_temp, k11_temp, k12_temp;
    logic                                        k9_val_en, k9_val_en_next, k12_val_en, k12_val_en_next, i2_val_en, i2_val_en_next;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0]       tile_idx_4, tile_idx_4_next, tile_idx_5, tile_idx_5_next, tile_idx_6, tile_idx_6_next;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]             select_data_4, select_data_4_next, select_data_5, select_data_5_next, select_data_6, select_data_6_next;
    `endif
    
    logic [(TABLE_BIT_WIDTH*2)-1:0] rd_x_data_next; // x table value
    logic [(TABLE_BIT_WIDTH*2)-1:0] rd_y_data_next; // y table value

    logic [TABLE_BIT_WIDTH-1:0]                  k1, k1_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  k2, k2_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  k3, k3_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  k4, k4_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  k5, k5_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  k6, k6_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  r1, r1_next; // 
    logic [TABLE_BIT_WIDTH-1:0]                  i1, i1_next; // 
    logic [TABLE_BIT_WIDTH+TABLE_FRAC_WIDTH-1:0] k4_temp, k5_temp, k6_temp;

    logic k3_val_en, k6_val_en, k6_val_en_next, i1_val_en, i1_val_en_next;

    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] tile_idx, tile_idx_2, tile_idx_2_next, tile_idx_3, tile_idx_3_next;
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       select_data, select_data_2, select_data_2_next, select_data_3, select_data_3_next;

    logic [TABLE_BIT_WIDTH-1:0] table_x_real, table_x_real_next, table_x_imag, table_x_imag_next; // 
    logic [TABLE_BIT_WIDTH-1:0] table_y_real, table_y_real_next, table_y_imag_next; // 


    // Instantiate the interp table array
    `ifdef USE_3D
    `ifdef USE_SRAM_16
    // TSMC 16nm
    sram_dp_uhde_32b interp_array_xy (

        // Inputs
        .CLK(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~(interp_table_consts_en_in | select_en_in)), // port A enable (active-low)
        .CENB(~select_en_in), // port B enable (active-low)
        .GWENA(~interp_table_consts_en_in), // write port A enable (active-low)
        .GWENB(1'b1), // write port B enable (active-low)
        .AA(interp_table_consts_en_in ? interp_table_consts_addr_in : select_addr_x_in), // port A address
        .AB(select_addr_y_in), // port B address
        .DA(interp_table_consts_in), // write port A data
        .DB(32'b0), // write port B data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for port A is generated directly from the high phase of the external clock input
        .STOVAB(1'b0), // When the STOV pin is HIGH and the STOVAB pin is LOW, the internal clock pulse for port B is generated directly from the high phase of the external clock input.
        .EMA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAP(1'b0), // default is LOW
        .EMAW(2'b01), // similar to EMA; try 01 (default)
        .EMAS(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .QA(rd_x_data_next), // output data for A
        .QB(rd_y_data_next) // output data for B

    );
    sram_sp_hde_32b interp_array_z (

        // Inputs
        .CLK(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CEN(~(interp_table_consts_en_in | i1_val_en)), // port enable (active-low)
        .GWEN(~interp_table_consts_en_in), // write port enable (active-low)
        .A(interp_table_consts_en_in ? interp_table_consts_addr_in : select_addr_z_3), // port address
        .D(interp_table_consts_in), // write port data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for the port is generated directly from the high phase of the external clock input
        .EMA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAW(2'b01), // similar to EMA; try 01 (default)
        .EMAS(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .Q(rd_z_data_next) // output data

    );
    `else
    mem_2rw #(
        .ENTRY_BIT_WIDTH(TABLE_BIT_WIDTH*2), // store the entire complex value in the same slot
        .NUM_ENTRIES(NUM_TABLE_ENTRIES)
        ) interp_array_xy (

        // Inputs
        .clock(clock),
        .init(init_mem),
        .rwa_en(interp_table_consts_en_in | select_en_in), // port A enable
        .rwb_en(select_en_in), // port B enable
        .wra_en(interp_table_consts_en_in), // port A write enable
        .wrb_en(1'b0), // port B write enable
        .rwa_idx(interp_table_consts_en_in ? interp_table_consts_addr_in : select_addr_x_in), // read A address
        .rwb_idx(select_addr_y_in), // read B address
        .wra_data(interp_table_consts_in), // port A write data
        .wrb_data(32'b0), // port B write data

        // Outputs
        .rda_out(rd_x_data_next), // read A data
        .rdb_out(rd_y_data_next) // read B data

    );
    mem_1rw #(
        .ENTRY_BIT_WIDTH(TABLE_BIT_WIDTH*2), // store the entire complex value in the same slot
        .NUM_ENTRIES(NUM_TABLE_ENTRIES)
        ) interp_array_z (

        // Inputs
        .clock(clock),
        .init(init_mem),
        .rwa_en(interp_table_consts_en_in | i1_val_en), // port A enable
        .wra_en(interp_table_consts_en_in), // port A write enable
        .rwa_idx(interp_table_consts_en_in ? interp_table_consts_addr_in : select_addr_z_3), // read A address
        .wra_data(interp_table_consts_in), // port A write data

        // Outputs
        .rda_out(rd_z_data_next) // read A data

    );
    `endif
    `else
    `ifdef USE_SRAM_16
    // TSMC 16nm
    sram_dp_uhde_32b interp_array_xy (

        // Inputs
        .CLK(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~(interp_table_consts_en_in | select_en_in)), // read port chip enable
        .CENB(~select_en_in), // write port chip enable
        .GWENA(~interp_table_consts_en_in), // read port chip enable
        .GWENB(1'b1), // write port chip enable
        .AA(interp_table_consts_en_in ? interp_table_consts_addr_in : select_addr_x_in), // read port address
        .AB(select_addr_y_in), // write port address
        .DA(interp_table_consts_in), // write port data
        .DB(32'b0), // write port data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for port A is generated directly from the high phase of the external clock input
        .STOVAB(1'b0), // When the STOV pin is HIGH and the STOVAB pin is LOW, the internal clock pulse for port B is generated directly from the high phase of the external clock input.
        .EMA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAP(1'b0), // default is LOW
        .EMAW(2'b01), // similar to EMA; try 01 (default)
        .EMAS(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .QA(rd_x_data_next), // output data for A
        .QB(rd_y_data_next) // output data for A

    );
    `else
    mem_2rw #(
        .ENTRY_BIT_WIDTH(TABLE_BIT_WIDTH*2), // store the entire complex value in the same slot
        .NUM_ENTRIES(NUM_TABLE_ENTRIES)
        ) interp_array_xy (

        // Inputs
        .clock(clock),
        .init(init_mem),
        .rwa_en(interp_table_consts_en_in | select_en_in), // port A enable
        .rwb_en(select_en_in), // port B enable
        .wra_en(interp_table_consts_en_in), // port A write enable
        .wrb_en(1'b0), // port B write enable
        .rwa_idx(interp_table_consts_en_in ? interp_table_consts_addr_in : select_addr_x_in), // read A address
        .rwb_idx(select_addr_y_in), // read B address
        .wra_data(interp_table_consts_in), // port A write data
        .wrb_data(32'b0), // port B write data

        // Outputs
        .rda_out(rd_x_data_next), // read A data
        .rdb_out(rd_y_data_next) // read B data

    );
    `endif
    `endif


    // Combine the x and y table values to create the final interpolation constant
    always_comb begin
        `ifdef USE_3D
        k9_val_en_next       = i1_val_en;
        k12_val_en_next      = k9_val_en;
        i2_val_en_next       = k12_val_en;
        tile_idx_4_next      = tile_idx_3;
        tile_idx_5_next      = tile_idx_4;
        tile_idx_6_next      = tile_idx_5;
        select_data_4_next   = select_data_3;
        select_data_5_next   = select_data_4;
        select_data_6_next   = select_data_5;
        r1_saved_next        = r1;
        i1_saved_next        = i1;
        select_addr_z_2_next = select_addr_z;
        select_addr_z_3_next = select_addr_z_2;
        `endif
        k6_val_en_next     = k3_val_en;
        i1_val_en_next     = k6_val_en;
        tile_idx_2_next    = tile_idx;
        tile_idx_3_next    = tile_idx_2;
        select_data_2_next = select_data;
        select_data_3_next = select_data_2;

        table_x_real_next = rd_x_data_next[(TABLE_BIT_WIDTH*2)-1:TABLE_BIT_WIDTH];
        table_x_imag_next = rd_x_data_next[TABLE_BIT_WIDTH-1:0];
        table_y_real_next = rd_y_data_next[(TABLE_BIT_WIDTH*2)-1:TABLE_BIT_WIDTH];
        table_y_imag_next = rd_y_data_next[TABLE_BIT_WIDTH-1:0];

        // Cycle 1
        k1_next = $signed(table_x_real_next) + $signed(table_x_imag_next); // x_real + x_imag
        k2_next = $signed(table_y_imag_next) - $signed(table_y_real_next); // y_imag - y_real
        k3_next = $signed(table_y_real_next) + $signed(table_y_imag_next); // y_real + y_imag

        // Cycle 2
        k4_temp = $signed(table_y_real) * $signed(k1); // y_real * k1
        k4_next = k4_temp[TABLE_BIT_WIDTH+TABLE_FRAC_WIDTH-1:TABLE_FRAC_WIDTH]; // y_real * k1
        k5_temp = $signed(table_x_real) * $signed(k2); // x_real * k2
        k5_next = k5_temp[TABLE_BIT_WIDTH+TABLE_FRAC_WIDTH-1:TABLE_FRAC_WIDTH]; // x_real * k2
        k6_temp = $signed(table_x_imag) * $signed(k3); // x_imag * k3
        k6_next = k6_temp[TABLE_BIT_WIDTH+TABLE_FRAC_WIDTH-1:TABLE_FRAC_WIDTH]; // x_imag * k3

        // Cycle 3
        r1_next = $signed(k4) - $signed(k6); // k4 - k6
        i1_next = $signed(k4) + $signed(k5); // k4 + k5

        `ifdef USE_3D
        table_z_real_next = rd_z_data_next[(TABLE_BIT_WIDTH*2)-1:TABLE_BIT_WIDTH];
        table_z_imag_next = rd_z_data_next[TABLE_BIT_WIDTH-1:0];

        // Cycle 4
        k7_next = $signed(r1) + $signed(i1); // r1 + i1
        k8_next = $signed(table_z_imag_next) - $signed(table_z_real_next); // z_imag - z_real
        k9_next = $signed(table_z_real_next) + $signed(table_z_imag_next); // z_real + z_imag

        // Cycle 5
        k10_temp = $signed(table_z_real) * $signed(k7); // z_real * k7
        k10_next = k10_temp[TABLE_BIT_WIDTH+TABLE_FRAC_WIDTH-1:TABLE_FRAC_WIDTH]; // z_real * k7
        k11_temp = $signed(r1_saved) * $signed(k8); // r1 * k8
        k11_next = k11_temp[TABLE_BIT_WIDTH+TABLE_FRAC_WIDTH-1:TABLE_FRAC_WIDTH]; // r1 * k8
        k12_temp = $signed(i1_saved) * $signed(k9); // i1 * k9
        k12_next = k12_temp[TABLE_BIT_WIDTH+TABLE_FRAC_WIDTH-1:TABLE_FRAC_WIDTH]; // i1 * k9

        // Cycle 6
        r2_next = $signed(k10) - $signed(k12); // k10 - k12
        i2_next = $signed(k10) + $signed(k11); // k10 + k11
        `endif
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            `ifdef USE_3D
            k9_val_en       <= `SD 0;
            k12_val_en      <= `SD 0;
            i2_val_en       <= `SD 0;
            tile_idx_4      <= `SD 0;
            tile_idx_5      <= `SD 0;
            tile_idx_6      <= `SD 0;
            select_data_4   <= `SD 0;
            select_data_5   <= `SD 0;
            select_data_6   <= `SD 0;
            table_z_real    <= `SD 0;
            k7              <= `SD 0;
            k8              <= `SD 0;
            k9              <= `SD 0;
            k10             <= `SD 0;
            k11             <= `SD 0;
            k12             <= `SD 0;
            select_addr_z   <= `SD 0;
            select_addr_z_2 <= `SD 0;
            select_addr_z_3 <= `SD 0;
            r1              <= `SD 0;
            r1_saved        <= `SD 0;
            i1              <= `SD 0;
            i1_saved        <= `SD 0;
            `endif

            k3_val_en     <= `SD 0;
            k6_val_en     <= `SD 0;
            i1_val_en     <= `SD 0;
            tile_idx      <= `SD 0;
            tile_idx_2    <= `SD 0;
            tile_idx_3    <= `SD 0;
            select_data   <= `SD 0;
            select_data_2 <= `SD 0;
            select_data_3 <= `SD 0;

            k1           <= `SD 0;
            k2           <= `SD 0;
            k3           <= `SD 0;
            k4           <= `SD 0;
            k5           <= `SD 0;
            k6           <= `SD 0;
            table_x_real <= `SD 0;
            table_x_imag <= `SD 0;
            table_y_real <= `SD 0;

            interp_table_real_out     <= `SD 0;
            interp_table_imag_out     <= `SD 0;
            interp_table_tile_idx_out <= `SD 0;
            interp_table_data_out     <= `SD 0;
            interp_table_en_out       <= `SD 0;
        end else begin
            `ifdef USE_3D
            k9_val_en       <= `SD k9_val_en_next;
            k12_val_en      <= `SD k12_val_en_next;
            i2_val_en       <= `SD i2_val_en_next;
            tile_idx_4      <= `SD tile_idx_4_next;
            tile_idx_5      <= `SD tile_idx_5_next;
            tile_idx_6      <= `SD tile_idx_6_next;
            select_data_4   <= `SD select_data_4_next;
            select_data_5   <= `SD select_data_5_next;
            select_data_6   <= `SD select_data_6_next;
            table_z_real    <= `SD table_z_real_next;
            k7              <= `SD k7_next;
            k8              <= `SD k8_next;
            k9              <= `SD k9_next;
            k10             <= `SD k10_next;
            k11             <= `SD k11_next;
            k12             <= `SD k12_next;
            select_addr_z   <= `SD select_addr_z_in;
            select_addr_z_2 <= `SD select_addr_z_2_next;
            select_addr_z_3 <= `SD select_addr_z_3_next;
            r1              <= `SD r1_next;
            r1_saved        <= `SD r1_saved_next;
            i1              <= `SD i1_next;
            i1_saved        <= `SD i1_saved_next;
            `endif

            k3_val_en     <= `SD select_en_in;
            k6_val_en     <= `SD k6_val_en_next;
            i1_val_en     <= `SD i1_val_en_next;
            tile_idx      <= `SD select_tile_idx_in;
            tile_idx_2    <= `SD tile_idx_2_next;
            tile_idx_3    <= `SD tile_idx_3_next;
            select_data   <= `SD select_data_in;
            select_data_2 <= `SD select_data_2_next;
            select_data_3 <= `SD select_data_3_next;

            k1           <= `SD k1_next;
            k2           <= `SD k2_next;
            k3           <= `SD k3_next;
            k4           <= `SD k4_next;
            k5           <= `SD k5_next;
            k6           <= `SD k6_next;
            table_x_real <= `SD table_x_real_next;
            table_x_imag <= `SD table_x_imag_next;
            table_y_real <= `SD table_y_real_next;

            `ifdef USE_3D
            interp_table_real_out     <= `SD i2_val_en ? r2_next : '0;
            interp_table_imag_out     <= `SD i2_val_en ? i2_next : '0;
            interp_table_tile_idx_out <= `SD i2_val_en ? tile_idx_6 : '0;
            interp_table_data_out     <= `SD i2_val_en ? select_data_6 : '0;
            interp_table_en_out       <= `SD i2_val_en;
            `else
            interp_table_real_out     <= `SD i1_val_en ? r1_next : '0;
            interp_table_imag_out     <= `SD i1_val_en ? i1_next : '0;
            interp_table_tile_idx_out <= `SD i1_val_en ? tile_idx_3 : '0;
            interp_table_data_out     <= `SD i1_val_en ? select_data_3 : '0;
            interp_table_en_out       <= `SD i1_val_en;
            `endif
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // interp_table
