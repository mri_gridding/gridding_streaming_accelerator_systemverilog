/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  mem_1r_1w.sv                                        //
//                                                                     //
//  Description :  This module creates the regfile for the interp      //
//                 table oversamp values.                              //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module mem_1r_1w #(
    parameter ENTRY_BIT_WIDTH = 64,
    `ifdef USE_3D_CUBE
    parameter NUM_ENTRIES     = 131072
    `else
    parameter NUM_ENTRIES     = 256
    `endif
    ) (

        // Inputs
        input                           clock,
        input                           init,
        input [$clog2(NUM_ENTRIES)-1:0] rd_idx, // read index
        input [$clog2(NUM_ENTRIES)-1:0] wr_idx,  // write index
        input [ENTRY_BIT_WIDTH-1:0]     wr_data, // write data
        input                           wr_en,

        // Outputs
        output logic [ENTRY_BIT_WIDTH-1:0] rd_out // read data

    );

    `ifndef SYNTH_3D
    logic [NUM_ENTRIES-1:0][ENTRY_BIT_WIDTH-1:0] registers; // registers

    wire [ENTRY_BIT_WIDTH-1:0] rd_reg = registers[rd_idx];

    //
    // Read port
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock)
        // if(wr_en && (wr_idx == rd_idx))
        //     rd_out <= `SD wr_data; // internal forwarding
        // else
            rd_out <= `SD rd_reg;

    //
    // Write port
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock)
        if(init)
            registers <= `SD '0;
        else
            if(wr_en)
                registers[wr_idx] <= `SD wr_data;
    `endif

endmodule // mem_1r_1w
