/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  select.sv                                           //
//                                                                     //
//  Description :  Calculates distance from pipeline to input, interp  //
//                 table indices, and tile index.                      //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module select #(
    `ifdef USE_3D
    parameter MAX_GRID_DIM_Z          = 1024,
    `ifdef USE_3D_CUBE
    parameter ACCEL_DIM_Z             = 8,
    parameter TILE_DIM_Z              = (MAX_GRID_DIM_Z/ACCEL_DIM_Z),
    `endif
    `endif
    parameter SAMPLE_BIT_WIDTH        = 32,
    parameter SAMPLE_COORD_BIT_WIDTH  = 32,
    parameter SAMPLE_COORD_FRAC_WIDTH = 22,
    parameter MAX_INTERP_WINDOW_SIZE  = 8,
    parameter TABLE_OVERSAMP          = 32,
    parameter ACCEL_DIM_X             = 8,
    parameter ACCEL_DIM_Y             = 8,
    parameter MAX_GRID_DIM_X          = 1024,
    parameter MAX_GRID_DIM_Y          = 1024,
    parameter TILE_DIM_X              = (MAX_GRID_DIM_X/ACCEL_DIM_X),
    parameter TILE_DIM_Y              = (MAX_GRID_DIM_Y/ACCEL_DIM_Y),
    parameter NUM_TABLE_ENTRIES       = (MAX_INTERP_WINDOW_SIZE*TABLE_OVERSAMP),
    parameter NUM_BUFFER_ENTRIES      = 256
    ) (

        // Inputs
        `ifdef USE_3D
        input [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_z_coord_in,
        `ifdef USE_3D_CUBE
        input [$clog2(ACCEL_DIM_Z)-1:0]            accel_z_idx_in,
        input [$clog2(TILE_DIM_Z)-1:0]             tile_z_dim_in,
        `else
        input [$clog2(MAX_GRID_DIM_Z)-1:0]         z_slice_idx_in,
        `endif
        `endif
        input                                      clock,
        input                                      reset,
        input [(SAMPLE_BIT_WIDTH*2)-1:0]           sample_data_in,
        input [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_x_coord_in,
        input [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_y_coord_in,
        input                                      sample_en_in,
        input [$clog2(ACCEL_DIM_X)-1:0]            accel_x_idx_in,
        input [$clog2(ACCEL_DIM_Y)-1:0]            accel_y_idx_in,
        input [$clog2(TILE_DIM_X)-1:0]             tile_x_dim_in,
        input [$clog2(TILE_DIM_Y)-1:0]             tile_y_dim_in,
        input [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0] window_dim_in,

        // Outputs
        `ifdef USE_3D
        output logic [$clog2(NUM_TABLE_ENTRIES)-1:0]  select_z_table_idx_out,
        `endif
        output logic [$clog2(NUM_TABLE_ENTRIES)-1:0]  select_x_table_idx_out,
        output logic [$clog2(NUM_TABLE_ENTRIES)-1:0]  select_y_table_idx_out,
        output logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] select_tile_idx_out,
        output logic [(SAMPLE_BIT_WIDTH*2)-1:0]       select_data_out,
        output logic                                  select_en_out

    );


    // Local variables
    `ifdef USE_3D
    logic [$clog2(NUM_TABLE_ENTRIES)-1:0]                                          z_table_idx_next;
    `ifdef USE_3D_CUBE
    logic [$clog2(ACCEL_DIM_Z)+SAMPLE_COORD_FRAC_WIDTH-1:0]                        z_distance, z_distance_next, z_distance_table_round;
    logic [$clog2(ACCEL_DIM_Z)+SAMPLE_COORD_FRAC_WIDTH-1:0]                        z_coord, z_coord_next;
    logic [$clog2(ACCEL_DIM_Z)-1:0]                                                accel_z_idx; // holds the z index of this pipeline in the accelerator grid
    logic [$clog2(TILE_DIM_Z)-1:0]                                                 tile_z_dim; // holds the z index of this pipeline in the accelerator grid
    logic [SAMPLE_COORD_BIT_WIDTH-SAMPLE_COORD_FRAC_WIDTH-$clog2(ACCEL_DIM_Z)-1:0] z_base_tile_idx, z_base_tile_idx_next;
    logic [$clog2(TILE_DIM_Z)-1:0]                                                 z_tile_idx, z_tile_idx_next;
    `else
    logic [$clog2(MAX_GRID_DIM_Z)+SAMPLE_COORD_FRAC_WIDTH-1:0]                     z_distance, z_distance_next, z_distance_table_round;
    logic [$clog2(MAX_GRID_DIM_Z)+SAMPLE_COORD_FRAC_WIDTH-1:0]                     z_coord, z_coord_next;
    logic [$clog2(MAX_GRID_DIM_Z)-1:0]                                             z_slice_idx; // holds the z index of this pipeline slice in the accelerator grid
    `endif
    `endif

    logic [$clog2(ACCEL_DIM_X)-1:0]            accel_x_idx; // holds the x index of this pipeline in the accelerator grid
    logic [$clog2(ACCEL_DIM_Y)-1:0]            accel_y_idx; // holds the y index of this pipeline in the accelerator grid
    logic [$clog2(TILE_DIM_X)-1:0]             tile_x_dim; // holds the x index of this pipeline in the accelerator grid
    logic [$clog2(TILE_DIM_Y)-1:0]             tile_y_dim; // holds the y index of this pipeline in the accelerator grid
    logic [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0] interp_window_dim;

    logic [SAMPLE_COORD_BIT_WIDTH-SAMPLE_COORD_FRAC_WIDTH-$clog2(ACCEL_DIM_X)-1:0] x_base_tile_idx, x_base_tile_idx_next;
    logic [SAMPLE_COORD_BIT_WIDTH-SAMPLE_COORD_FRAC_WIDTH-$clog2(ACCEL_DIM_Y)-1:0] y_base_tile_idx, y_base_tile_idx_next;
    logic [$clog2(ACCEL_DIM_X)+SAMPLE_COORD_FRAC_WIDTH-1:0]                        x_coord, x_coord_next;
    logic [$clog2(ACCEL_DIM_Y)+SAMPLE_COORD_FRAC_WIDTH-1:0]                        y_coord, y_coord_next;
    logic [$clog2(ACCEL_DIM_X)+SAMPLE_COORD_FRAC_WIDTH-1:0]                        x_distance, x_distance_next, x_distance_table_round;
    logic [$clog2(ACCEL_DIM_Y)+SAMPLE_COORD_FRAC_WIDTH-1:0]                        y_distance, y_distance_next, y_distance_table_round;

    logic [$clog2(TILE_DIM_X)-1:0]         x_tile_idx, x_tile_idx_next;
    logic [$clog2(TILE_DIM_Y)-1:0]         y_tile_idx, y_tile_idx_next;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] xy_tile_idx_next;
    logic [$clog2(NUM_TABLE_ENTRIES)-1:0]  x_table_idx_next;
    logic [$clog2(NUM_TABLE_ENTRIES)-1:0]  y_table_idx_next;

    logic pipeline_affected_next;

    logic [(SAMPLE_BIT_WIDTH*2)-1:0] sample_saved, sample_saved_2, sample_saved_2_next, sample_saved_3, sample_saved_3_next;
    logic                            sample_en_saved, sample_en_saved_2, sample_en_saved_2_next, sample_en_saved_3, sample_en_saved_3_next;


    // Combine the x and y table values to create the final interpolation constant
    always_comb begin
        `ifdef USE_3D
        z_coord_next           = 0;
        z_distance_next        = 0;
        z_table_idx_next       = 0;
        z_distance_table_round = 0;
        `ifdef USE_3D_CUBE
        z_tile_idx_next        = 0;
        z_base_tile_idx_next   = 0;
        `endif
        `endif

        x_base_tile_idx_next   = 0;
        y_base_tile_idx_next   = 0;
        x_coord_next           = 0;
        y_coord_next           = 0;

        x_distance_next = 0;
        y_distance_next = 0;
        x_tile_idx_next = 0;
        y_tile_idx_next = 0;

        xy_tile_idx_next = 0;
        x_table_idx_next = 0;
        y_table_idx_next = 0;

        sample_saved_2_next    = sample_saved;
        sample_saved_3_next    = sample_saved_2;
        sample_en_saved_2_next = sample_en_saved;
        sample_en_saved_3_next = sample_en_saved_2;

        pipeline_affected_next = 0;
        x_distance_table_round = 0;
        y_distance_table_round = 0;

        // Cycle 1
        if(sample_en_in) begin
            // Get the quotient and remainder to determine grid and tile location
            `ifdef USE_3D_CUBE
            z_base_tile_idx_next = sample_z_coord_in[SAMPLE_COORD_BIT_WIDTH-1:$clog2(ACCEL_DIM_Z)+SAMPLE_COORD_FRAC_WIDTH]; // z virtual tile index
            `endif
            x_base_tile_idx_next = sample_x_coord_in[SAMPLE_COORD_BIT_WIDTH-1:$clog2(ACCEL_DIM_X)+SAMPLE_COORD_FRAC_WIDTH]; // x virtual tile index
            y_base_tile_idx_next = sample_y_coord_in[SAMPLE_COORD_BIT_WIDTH-1:$clog2(ACCEL_DIM_Y)+SAMPLE_COORD_FRAC_WIDTH]; // y virtual tile index
            
            // Extract the "relative" coordinates (within a tile)
            `ifdef USE_3D
            `ifdef USE_3D_CUBE
            z_coord_next         = sample_z_coord_in[$clog2(ACCEL_DIM_Z)+SAMPLE_COORD_FRAC_WIDTH-1:0]; // "relative" z coordinate (within a tile)
            `else
            z_coord_next         = sample_z_coord_in[$clog2(MAX_GRID_DIM_Z)+SAMPLE_COORD_FRAC_WIDTH-1:0]; // "relative" z coordinate (within a tile)
            `endif
            `endif
            x_coord_next         = sample_x_coord_in[$clog2(ACCEL_DIM_X)+SAMPLE_COORD_FRAC_WIDTH-1:0]; // "relative" x coordinate (within a tile)
            y_coord_next         = sample_y_coord_in[$clog2(ACCEL_DIM_Y)+SAMPLE_COORD_FRAC_WIDTH-1:0]; // "relative" y coordinate (within a tile)
        end

        // Cycle 2
        if(sample_en_saved) begin
            // Calculate the x and y distances (from the pipeline to the relative coordinates)
            `ifdef USE_3D
            `ifdef USE_3D_CUBE
            z_distance_next = ({ACCEL_DIM_Z, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}} + z_coord) - {accel_z_idx, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}; // shortest distance around the torus in the z dimension
            `else
            z_distance_next = ({MAX_GRID_DIM_Z, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}} + z_coord) - {z_slice_idx, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}; // shortest distance around the torus in the z dimension
            `endif
            `endif
            x_distance_next = ({ACCEL_DIM_X, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}} + x_coord) - {accel_x_idx, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}; // shortest distance around the torus in the x dimension
            y_distance_next = ({ACCEL_DIM_Y, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}} + y_coord) - {accel_y_idx, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}; // shortest distance around the torus in the y dimension
            
            // Check if a wrap occured in the x dimension (virtual tile)
            if(x_coord < {accel_x_idx, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}) begin
                if(x_base_tile_idx == 0) begin
                    x_tile_idx_next = tile_x_dim - 1;
                end else begin
                    x_tile_idx_next = x_base_tile_idx - 1;
                end
            end else begin
                x_tile_idx_next = x_base_tile_idx;
            end
            
            // Check if a wrap occured in the y dimension (virtual tile)
            if(y_coord < {accel_y_idx, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}) begin
                if(y_base_tile_idx == 0) begin
                    y_tile_idx_next = tile_y_dim - 1;
                end else begin
                    y_tile_idx_next = y_base_tile_idx - 1;
                end
            end else begin
                y_tile_idx_next = y_base_tile_idx;
            end
            
            `ifdef USE_3D_CUBE
            // Check if a wrap occured in the z dimension (virtual tile)
            if(z_coord < {accel_z_idx, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}) begin
                if(z_base_tile_idx == 0) begin
                    z_tile_idx_next = tile_z_dim - 1;
                end else begin
                    z_tile_idx_next = z_base_tile_idx - 1;
                end
            end else begin
                z_tile_idx_next = z_base_tile_idx;
            end
            `endif
        end

        // Cycle 3
        if(sample_en_saved_2) begin
            // Check if the pipeline is affected
            `ifdef USE_3D
            if((x_distance < {interp_window_dim, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}) && (y_distance < {interp_window_dim, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}) && (z_distance < {interp_window_dim, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}})) begin
            `else
            if((x_distance < {interp_window_dim, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}}) && (y_distance < {interp_window_dim, {SAMPLE_COORD_FRAC_WIDTH{1'b0}}})) begin
            `endif
                pipeline_affected_next = 1;
            end
            
            // Calculate the combined tile index ("depth" in the pipeline's data array)
            // TODO: figure out how to remove this multiplier when TILE_DIM_X isn't a power of 2
            `ifdef USE_3D_CUBE
            xy_tile_idx_next = (z_tile_idx * tile_x_dim * tile_y_dim) + (y_tile_idx * tile_x_dim) + x_tile_idx; // 1D global virtual tile index
            `else
            xy_tile_idx_next = (y_tile_idx * tile_x_dim) + x_tile_idx; // 1D global virtual tile index
            `endif
            
            // Calculate the table index (based on distance)
            `ifdef USE_3D
            z_distance_table_round = z_distance + {1'b1, {(SAMPLE_COORD_FRAC_WIDTH-$clog2(TABLE_OVERSAMP)-1){1'b0}}};
            `ifdef USE_3D_CUBE
            z_table_idx_next       = z_distance_table_round[$clog2(ACCEL_DIM_Z)+SAMPLE_COORD_FRAC_WIDTH-1:SAMPLE_COORD_FRAC_WIDTH-$clog2(TABLE_OVERSAMP)]; // *L (a power of 2) is implemented as a left shift
            `else
            z_table_idx_next       = z_distance_table_round[$clog2(MAX_GRID_DIM_Z)+SAMPLE_COORD_FRAC_WIDTH-1:SAMPLE_COORD_FRAC_WIDTH-$clog2(TABLE_OVERSAMP)]; // *L (a power of 2) is implemented as a left shift
            `endif
            `endif
            x_distance_table_round = x_distance + {1'b1, {(SAMPLE_COORD_FRAC_WIDTH-$clog2(TABLE_OVERSAMP)-1){1'b0}}};
            y_distance_table_round = y_distance + {1'b1, {(SAMPLE_COORD_FRAC_WIDTH-$clog2(TABLE_OVERSAMP)-1){1'b0}}};
            x_table_idx_next       = x_distance_table_round[$clog2(ACCEL_DIM_X)+SAMPLE_COORD_FRAC_WIDTH-1:SAMPLE_COORD_FRAC_WIDTH-$clog2(TABLE_OVERSAMP)]; // *L (a power of 2) is implemented as a left shift
            y_table_idx_next       = y_distance_table_round[$clog2(ACCEL_DIM_Y)+SAMPLE_COORD_FRAC_WIDTH-1:SAMPLE_COORD_FRAC_WIDTH-$clog2(TABLE_OVERSAMP)]; // *L (a power of 2) is implemented as a left shift
        end
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            `ifdef USE_3D
            z_coord                <= `SD 0;
            z_distance             <= `SD 0;
            select_z_table_idx_out <= `SD 0;
            `ifdef USE_3D_CUBE
            accel_z_idx            <= `SD accel_z_idx_in;
            tile_z_dim             <= `SD tile_z_dim_in;
            z_base_tile_idx        <= `SD 0;
            z_tile_idx             <= `SD 0;
            `else
            z_slice_idx            <= `SD z_slice_idx_in;
            `endif
            `endif

            accel_x_idx            <= `SD accel_x_idx_in;
            accel_y_idx            <= `SD accel_y_idx_in;
            tile_x_dim             <= `SD tile_x_dim_in;
            tile_y_dim             <= `SD tile_y_dim_in;
            interp_window_dim      <= `SD window_dim_in;

            x_base_tile_idx <= `SD 0;
            y_base_tile_idx <= `SD 0;
            x_coord         <= `SD 0;
            y_coord         <= `SD 0;

            x_distance <= `SD 0;
            y_distance <= `SD 0;
            x_tile_idx <= `SD 0;
            y_tile_idx <= `SD 0;

            sample_saved      <= `SD 0;
            sample_saved_2    <= `SD 0;
            sample_saved_3    <= `SD 0;
            sample_en_saved   <= `SD 0;
            sample_en_saved_2 <= `SD 0;
            sample_en_saved_3 <= `SD 0;

            select_x_table_idx_out <= `SD 0;
            select_y_table_idx_out <= `SD 0;
            select_tile_idx_out    <= `SD 0;
            select_data_out        <= `SD 0;
            select_en_out          <= `SD 0;
        end else begin
            `ifdef USE_3D
            z_coord                <= `SD z_coord_next;
            z_distance             <= `SD z_distance_next;
            select_z_table_idx_out <= `SD (sample_en_saved_3_next & pipeline_affected_next) ? z_table_idx_next : '0;
            `ifdef USE_3D_CUBE
            z_base_tile_idx        <= `SD z_base_tile_idx_next;
            z_tile_idx             <= `SD z_tile_idx_next;
            `endif
            `endif

            x_base_tile_idx        <= `SD x_base_tile_idx_next;
            y_base_tile_idx        <= `SD y_base_tile_idx_next;
            x_coord                <= `SD x_coord_next;
            y_coord                <= `SD y_coord_next;

            x_distance <= `SD x_distance_next;
            y_distance <= `SD y_distance_next;
            x_tile_idx <= `SD x_tile_idx_next;
            y_tile_idx <= `SD y_tile_idx_next;

            sample_saved      <= `SD sample_data_in;
            sample_saved_2    <= `SD sample_saved_2_next;
            sample_saved_3    <= `SD sample_saved_3_next;
            sample_en_saved   <= `SD sample_en_in;
            sample_en_saved_2 <= `SD sample_en_saved_2_next;
            sample_en_saved_3 <= `SD sample_en_saved_3_next;

            select_x_table_idx_out <= `SD (sample_en_saved_3_next & pipeline_affected_next) ? x_table_idx_next : '0;
            select_y_table_idx_out <= `SD (sample_en_saved_3_next & pipeline_affected_next) ? y_table_idx_next : '0;
            select_tile_idx_out    <= `SD (sample_en_saved_3_next & pipeline_affected_next) ? xy_tile_idx_next : '0;
            select_data_out        <= `SD (sample_en_saved_3_next & pipeline_affected_next) ? sample_saved_3_next : '0;
            select_en_out          <= `SD (sample_en_saved_3_next & pipeline_affected_next);
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // select
