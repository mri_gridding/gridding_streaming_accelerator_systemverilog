/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  accum.sv                                            //
//                                                                     //
//  Description :  Accumulates samples after interpolation.            //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module accum #(
    parameter SAMPLE_BIT_WIDTH   = 32,
    `ifdef USE_3D_CUBE
    parameter NUM_BANKS          = 16,
    parameter NUM_BANK_ENTRIES   = 131072,
    `else
    parameter NUM_BANKS          = 4,
    parameter NUM_BANK_ENTRIES   = 4096,
    `endif
    parameter NUM_BUFFER_ENTRIES = NUM_BANKS*NUM_BANK_ENTRIES
    ) (

        // Inputs
        input                                  clock,
        input                                  reset,
        input                                  init_mem,
        input [$clog2(NUM_BUFFER_ENTRIES)-1:0] interp_tile_idx_in, // read/write tile address (can be used for reading final values out)
        input [(SAMPLE_BIT_WIDTH*2)-1:0]       interp_data_in, // write data
        input                                  interp_en_in, // read/write enable
        input                                  accum_rd_en_in, // output a value (for reading final values out)

        // Outputs
        output logic [(SAMPLE_BIT_WIDTH*2)-1:0] accum_data_out,
        output logic                            accum_en_out

    );


    // Local variables
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       rd_data_next; // read value
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       interp_data; // saved input value
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0] interp_addr, wr_addr, wr_addr_next; // read value
    logic                                  interp_en, wr_en, wr_en_next, forward_en; // saved read/write enable
    logic                                  rd_en_saved; // saved read out enable
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]       wr_data, wr_data_next, forward_data;


    // Instantiate the accumulation array for this pipeline
    `ifdef USE_ACCUM_BANK
    logic [$clog2(NUM_BANKS)-1:0]                       bank_rd_idx, bank_rd_idx_saved, bank_wr_idx;
    logic [NUM_BANKS-1:0][(SAMPLE_BIT_WIDTH*2)-1:0]     bank_rd_data_next; // read value
    logic [NUM_BANKS-1:0][$clog2(NUM_BANK_ENTRIES)-1:0] bank_rd_addr, bank_wr_addr; // read value
    logic [NUM_BANKS-1:0]                               bank_wr_en; // saved read/write enable
    logic [NUM_BANKS-1:0]                               bank_rd_en; // saved read out enable
    logic [NUM_BANKS-1:0][(SAMPLE_BIT_WIDTH*2)-1:0]     bank_wr_data;
    

    `ifdef USE_ACCUM_SRAM_16
    // TSMC 16nm
    sram_2p_uhde_64b bank_array [NUM_BANKS-1:0] (

        // Inputs
        .CLK(clock), // clock - ADD "~" FOR SIMULATION; REMOVE FOR SYNTHESIS
        .CENA(~bank_rd_en), // read port chip enable
        .CENB(~bank_wr_en), // write port chip enable
        .AA(bank_rd_addr), // read port address
        .AB(bank_wr_addr), // write port address
        .DB(bank_wr_data), // write port data
        .STOV(1'b0), // When the STOV and STOVAB pins are HIGH, the internal clock pulse for port A is generated directly from the high phase of the external clock input
        .STOVAB(1'b0), // When the STOV pin is HIGH and the STOVAB pin is LOW, the internal clock pulse for port B is generated directly from the high phase of the external clock input.
        .EMA(3'b010), // extra margin adjustment - Austin uses 010; try 010 (default)
        .EMAW(2'b01), // similar to EMA; try 01 (default)
        .EMAS(1'b0), // default is LOW
        .EMAP(1'b0), // default is LOW
        .RET1N(1'b1), // retention mode; try 1?

        // Outputs
        .QA(bank_rd_data_next) // output data for A

    );
    `else
    mem_1r_1w #(
        .ENTRY_BIT_WIDTH((SAMPLE_BIT_WIDTH*2)),
        .NUM_ENTRIES(NUM_BANK_ENTRIES)
        ) bank_array [NUM_BANKS-1:0] (

        // Inputs
        .clock(clock),
        .init(init_mem),
        .rd_idx(bank_rd_addr), // read address
        .wr_idx(bank_wr_addr), // write address
        .wr_data(bank_wr_data), // write data
        .wr_en(bank_wr_en), // write enable

        // Outputs
        .rd_out(bank_rd_data_next) // read data

    );
    `endif
    `else
    mem_1r_1w #(
        .ENTRY_BIT_WIDTH((SAMPLE_BIT_WIDTH*2)),
        .NUM_ENTRIES(NUM_BUFFER_ENTRIES)
        ) data_array (

        // Inputs
        .clock(clock),
        .init(init_mem),
        .rd_idx(interp_tile_idx_in), // read address
        .wr_idx(wr_addr), // write address
        .wr_data(wr_data), // write data
        .wr_en(wr_en), // write enable

        // Outputs
        .rd_out(rd_data_next) // read data

    );
    `endif


    // Accumulate the interpolation result with the current grid value
    always_comb begin
        `ifdef USE_ACCUM_BANK
        bank_wr_idx = wr_addr[$clog2(NUM_BUFFER_ENTRIES)-1:$clog2(NUM_BUFFER_ENTRIES)-$clog2(NUM_BANKS)];
        bank_rd_idx = interp_tile_idx_in[$clog2(NUM_BUFFER_ENTRIES)-1:$clog2(NUM_BUFFER_ENTRIES)-$clog2(NUM_BANKS)];

        bank_rd_addr = 0;
        bank_rd_addr[bank_rd_idx] = interp_tile_idx_in[$clog2(NUM_BUFFER_ENTRIES)-$clog2(NUM_BANKS)-1:0];
        bank_rd_en   = 0;
        bank_rd_en[bank_rd_idx]   = interp_en_in | accum_rd_en_in;

        bank_wr_addr = 0;
        bank_wr_addr[bank_wr_idx] = wr_addr[$clog2(NUM_BUFFER_ENTRIES)-$clog2(NUM_BANKS)-1:0];
        bank_wr_data = 0;
        bank_wr_data[bank_wr_idx] = wr_data;
        bank_wr_en   = 0;
        bank_wr_en[bank_wr_idx]   = wr_en;

        rd_data_next = bank_rd_data_next[bank_rd_idx_saved];
        `endif

        wr_addr_next = interp_addr;
        wr_en_next   = interp_en;

        // Cycle 1 - Read value from the data array

        // Cycle 2 - Add input to the read value (check if read address is the same as previous; forward if needed)
        if(interp_addr == wr_addr) begin
            // Current write address matches saved read address; forward data!
            wr_data_next[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH] = $signed(wr_data[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH]) + $signed(interp_data[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH]);
            wr_data_next[SAMPLE_BIT_WIDTH-1:0]                    = $signed(wr_data[SAMPLE_BIT_WIDTH-1:0]) + $signed(interp_data[SAMPLE_BIT_WIDTH-1:0]);
        end else if(forward_en) begin
            wr_data_next[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH] = $signed(forward_data[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH]) + $signed(interp_data[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH]);
            wr_data_next[SAMPLE_BIT_WIDTH-1:0]                    = $signed(forward_data[SAMPLE_BIT_WIDTH-1:0]) + $signed(interp_data[SAMPLE_BIT_WIDTH-1:0]);
        end else begin
            wr_data_next[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH] = $signed(rd_data_next[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH]) + $signed(interp_data[(SAMPLE_BIT_WIDTH*2)-1:SAMPLE_BIT_WIDTH]);
            wr_data_next[SAMPLE_BIT_WIDTH-1:0]                    = $signed(rd_data_next[SAMPLE_BIT_WIDTH-1:0]) + $signed(interp_data[SAMPLE_BIT_WIDTH-1:0]);
        end

        // Cycle 3 - Write value to data array
    end // always_comb

    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(reset) begin
            rd_en_saved  <= `SD 0;
            interp_data  <= `SD 0;
            interp_addr  <= `SD 0;
            interp_en    <= `SD 0;
            wr_data      <= `SD 0;
            wr_addr      <= `SD 0;
            wr_en        <= `SD 0;
            forward_en   <= `SD 0;
            forward_data <= `SD 0;

            `ifdef USE_ACCUM_BANK
            bank_rd_idx_saved <= `SD 0;
            `endif

            accum_data_out <= `SD 0;
            accum_en_out   <= `SD 0;
        end else begin
            rd_en_saved  <= `SD accum_rd_en_in;
            interp_data  <= `SD interp_data_in;
            interp_addr  <= `SD interp_tile_idx_in;
            interp_en    <= `SD interp_en_in;
            wr_data      <= `SD wr_data_next;
            wr_addr      <= `SD wr_addr_next;
            wr_en        <= `SD wr_en_next;
            forward_en   <= `SD (wr_en && (interp_tile_idx_in == wr_addr)) ? 1 : 0;
            forward_data <= `SD wr_data;

            `ifdef USE_ACCUM_BANK
            bank_rd_idx_saved <= `SD bank_rd_idx;
            `endif

            accum_data_out <= `SD rd_en_saved ? rd_data_next : 0;
            accum_en_out   <= `SD rd_en_saved;
        end // if(reset) begin
    end // always_ff @(posedge clock)
endmodule // accum
