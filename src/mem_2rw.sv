/////////////////////////////////////////////////////////////////////////
//                                                                     //
//   Modulename :  mem_2rw.sv                                          //
//                                                                     //
//  Description :  This module creates the regfile for the interp      //
//                 table oversamp values.                              //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

// `timescale 1ns/100ps

module mem_2rw #(
    parameter ENTRY_BIT_WIDTH = 32,
    parameter NUM_ENTRIES     = 256
    ) (

        // Inputs
        input                           clock,
        input                           init,
        input                           rwa_en, // port A enable
        input                           rwb_en, // port B enable
        input                           wra_en, // write A enable
        input                           wrb_en, // write B enable
        input [$clog2(NUM_ENTRIES)-1:0] rwa_idx, // read A index
        input [$clog2(NUM_ENTRIES)-1:0] rwb_idx, // read B index
        input [ENTRY_BIT_WIDTH-1:0]     wra_data, // write data
        input [ENTRY_BIT_WIDTH-1:0]     wrb_data, // write data

        // Outputs
        output logic [ENTRY_BIT_WIDTH-1:0] rda_out, // read A data
        output logic [ENTRY_BIT_WIDTH-1:0] rdb_out  // read B data

    );

    logic [NUM_ENTRIES-1:0][ENTRY_BIT_WIDTH-1:0] registers; // registers

    wire [ENTRY_BIT_WIDTH-1:0] rda_reg = registers[rwa_idx];
    wire [ENTRY_BIT_WIDTH-1:0] rdb_reg = registers[rwb_idx];

    //
    // Read/Write ports A & B
    //
    // synopsys sync_set_reset "reset"
    always_ff @(posedge clock) begin
        if(init)
            registers <= `SD '0;

        if(rwa_en)
            if(wra_en)
                registers[rwa_idx] <= `SD wra_data;
            else
                rda_out <= `SD rda_reg;

        if(rwb_en)
            if(wrb_en)
                registers[rwb_idx] <= `SD wrb_data;
            else
                rdb_out <= `SD rdb_reg;
    end

endmodule // mem_2rw
