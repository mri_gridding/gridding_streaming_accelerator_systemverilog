/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  testbench.sv                                        //
//                                                                     //
//  Description :  Testbench for beamforming pipeline; this reads      //
//                 data files and distributes data to various modules. //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps
`ifndef USE_3D_CUBE
`define LOOP_SLICES
`endif

module testbench;

    // Parameters
    parameter INPUT_BIT_WIDTH        = 32; // used as a #define for input files
    parameter ACCEL_DIM_X            = 8; // number of pipelines in the x dimension
    parameter ACCEL_DIM_Y            = 8; // number of pipelines in the y dimension
    parameter ACCEL_DIM_Z            = 8; // number of pipelines in the z dimension
    parameter MAX_GRID_DIM_X         = 1024; // max size of the grid in the x dimension
    parameter MAX_GRID_DIM_Y         = 1024; // max size of the grid in the y dimension
    parameter MAX_GRID_DIM_Z         = 1024; // max size of the grid in the z dimension
    parameter TILE_DIM_X             = (MAX_GRID_DIM_X/ACCEL_DIM_X);
    parameter TILE_DIM_Y             = (MAX_GRID_DIM_Y/ACCEL_DIM_Y);
    parameter TILE_DIM_Z             = (MAX_GRID_DIM_Z/ACCEL_DIM_Z);
    parameter MAX_INTERP_WINDOW_SIZE = ACCEL_DIM_X; // max size of the interpolation window in any single dimension
    parameter SAMPLE_BIT_WIDTH       = 32;
    parameter SAMPLE_COORD_BIT_WIDTH = 32;
    parameter TABLE_BIT_WIDTH        = 16;
    parameter TABLE_FRAC_WIDTH       = 14;
    parameter TABLE_OVERSAMP         = 32; // number of table samples between each whole coordinate
    parameter NUM_TABLE_ENTRIES      = (MAX_INTERP_WINDOW_SIZE*TABLE_OVERSAMP);
    `ifdef USE_3D_CUBE
    parameter NUM_BANKS              = 16; // number of banks in each pipeline's data buffer
    parameter NUM_BANK_ENTRIES       = 131072; // number of entries in each data buffer bank
    `else
    parameter NUM_BANKS              = 4; // number of banks in each pipeline's data buffer
    parameter NUM_BANK_ENTRIES       = 4096; // number of entries in each data buffer bank
    `endif
    parameter NUM_BUFFER_ENTRIES     = (NUM_BANKS*NUM_BANK_ENTRIES); // number of entries in each pipeline's data buffer

    `ifdef USE_3D
    localparam INPUT_DATA_FOLDER     = "input_data_3d";
    localparam OUTPUT_DATA_FOLDER    = "output_data_3d";
    `else
    localparam INPUT_DATA_FOLDER     = "input_data";
    localparam OUTPUT_DATA_FOLDER    = "output_data";
    `endif
    localparam NUM_INPUT_POINTS_SIM  = 460800;
    localparam Z_SLICE_IDX_SIM       = 0;
    localparam X_GRID_DIM_SIM        = 768;
    localparam Y_GRID_DIM_SIM        = 768;
    localparam Z_GRID_DIM_SIM        = 768;
    localparam X_TILE_DIM_SIM        = (X_GRID_DIM_SIM/ACCEL_DIM_X);
    localparam Y_TILE_DIM_SIM        = (Y_GRID_DIM_SIM/ACCEL_DIM_Y);
    localparam Z_TILE_DIM_SIM        = (Z_GRID_DIM_SIM/ACCEL_DIM_Z);
    localparam INTERP_WINDOW_DIM_SIM = 6;
    localparam NUM_TABLE_DATA        = 193; // number of table entries in the input file
    `ifdef USE_3D_CUBE
    localparam NUM_BUFFER_DATA       = (X_GRID_DIM_SIM*Y_GRID_DIM_SIM*Z_GRID_DIM_SIM)/(ACCEL_DIM_X*ACCEL_DIM_Y*ACCEL_DIM_Z);
    `else
    localparam NUM_BUFFER_DATA       = (X_GRID_DIM_SIM*Y_GRID_DIM_SIM)/(ACCEL_DIM_X*ACCEL_DIM_Y);
    `endif

    // Testbench Signals
    logic        clock;
    logic        reset;
    logic [31:0] clock_count;

    // Pipeline Signals

    // Inputs
    `ifdef USE_3D
    `endif
    logic [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_z_coord_in;
    `ifdef USE_3D_CUBE
    logic [$clog2(TILE_DIM_Z)-1:0]             tile_z_dim_in;
    `else
    logic [$clog2(MAX_GRID_DIM_Z)-1:0]         z_slice_idx_in;
    `endif
    logic                                      init_mem;
    logic [$clog2(TILE_DIM_X)-1:0]             tile_x_dim_in;
    logic [$clog2(TILE_DIM_Y)-1:0]             tile_y_dim_in;
    logic [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0] window_dim_in;
    logic [(TABLE_BIT_WIDTH*2)-1:0]            interp_table_consts_in;
    logic [$clog2(NUM_TABLE_ENTRIES)-1:0]      interp_table_consts_addr_in;
    logic                                      interp_table_consts_en_in;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0]     rd_out_data_addr_in; // read/write tile address (can be used for reading final values out)
    logic                                      rd_out_data_en_in; // output a value (for reading final values out)
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]           sample_data_in;
    logic [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_x_coord_in;
    logic [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_y_coord_in;
    logic                                      sample_en_in;

    // Outputs
    `ifdef USE_3D_CUBE
    logic [ACCEL_DIM_X-1:0][ACCEL_DIM_Y-1:0][ACCEL_DIM_Z-1:0][(SAMPLE_BIT_WIDTH*2)-1:0] data_out;
    logic [ACCEL_DIM_X-1:0][ACCEL_DIM_Y-1:0][ACCEL_DIM_Z-1:0]                           data_en_out;
    `else
    logic [ACCEL_DIM_X-1:0][ACCEL_DIM_Y-1:0][(SAMPLE_BIT_WIDTH*2)-1:0] data_out;
    logic [ACCEL_DIM_X-1:0][ACCEL_DIM_Y-1:0]                           data_en_out;
    `endif


    // Instantiate the pipeline wrapper
    pipeline_wrapper #(
        `ifdef USE_3D
        .MAX_GRID_DIM_Z(MAX_GRID_DIM_Z),
        `ifdef USE_3D_CUBE
        .ACCEL_DIM_Z(ACCEL_DIM_Z),
        .TILE_DIM_Z(TILE_DIM_Z),
        `endif
        `endif
        .ACCEL_DIM_X(ACCEL_DIM_X),
        .ACCEL_DIM_Y(ACCEL_DIM_Y),
        .MAX_GRID_DIM_X(MAX_GRID_DIM_X),
        .MAX_GRID_DIM_Y(MAX_GRID_DIM_Y),
        .TILE_DIM_X(TILE_DIM_X),
        .TILE_DIM_Y(TILE_DIM_Y),
        .MAX_INTERP_WINDOW_SIZE(MAX_INTERP_WINDOW_SIZE),
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .SAMPLE_COORD_BIT_WIDTH(SAMPLE_COORD_BIT_WIDTH),
        .TABLE_BIT_WIDTH(TABLE_BIT_WIDTH),
        .TABLE_OVERSAMP(TABLE_OVERSAMP),
        .NUM_TABLE_ENTRIES(NUM_TABLE_ENTRIES),
        .NUM_BANKS(NUM_BANKS),
        .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES)
        ) pipeline_wrap_0 (

        // Inputs
        `ifdef USE_3D
        .sample_z_coord_in(sample_z_coord_in),
        `ifdef USE_3D_CUBE
        .tile_z_dim_in(tile_z_dim_in),
        `else
        .z_slice_idx_in(z_slice_idx_in),
        `endif
        `endif
        .clock(clock),
        .reset(reset),
        .init_mem(init_mem),
        .tile_x_dim_in(tile_x_dim_in),
        .tile_y_dim_in(tile_y_dim_in),
        .window_dim_in(window_dim_in),
        .interp_table_consts_in(interp_table_consts_in),
        .interp_table_consts_addr_in(interp_table_consts_addr_in),
        .interp_table_consts_en_in(interp_table_consts_en_in),
        .rd_out_data_addr_in(rd_out_data_addr_in),
        .rd_out_data_en_in(rd_out_data_en_in),
        .sample_data_in(sample_data_in),
        .sample_x_coord_in(sample_x_coord_in),
        .sample_y_coord_in(sample_y_coord_in),
        .sample_en_in(sample_en_in),

        // Outputs
        .data_out(data_out),
        .data_en_out(data_en_out)

    );

    `ifdef USE_3D
    integer file_input_coord_z; // input file descriptor
    logic [31:0] input_coord_z; // assumes the file always contains 32-bit integers
    `endif
    integer ppfile; // output file descriptor
    integer testfile; // output file descriptor
    integer output_file; // output file descriptor
    integer fileID; // output file descriptor
    integer file_input_coord_x; // input file descriptor
    integer file_input_coord_y; // input file descriptor
    integer file_input_data; // input file descriptor
    integer file_input_data_real; // input file descriptor
    integer file_input_data_imag; // input file descriptor
    integer file_table_data_real; // input file descriptor
    integer file_table_data_imag; // input file descriptor
    integer code; // return code for fscanf
    logic [63:0] input_data; // assumes the file always contains 32-bit integers
    logic [31:0] input_data_real; // assumes the file always contains 32-bit integers
    logic [31:0] input_data_imag; // assumes the file always contains 32-bit integers
    logic [31:0] input_coord_x; // assumes the file always contains 32-bit integers
    logic [31:0] input_coord_y; // assumes the file always contains 32-bit integers
    logic [31:0] table_data; // assumes the file always contains 32-bit integers

    // Generate System Clock
    always begin
        #(`VERILOG_CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    // Count the number of posedges and number of instructions completed
    // till simulation ends
    always @(posedge clock) begin
        if(reset) begin
            clock_count <= `SD 0;
        end else begin
            clock_count <= `SD (clock_count + 1);
        end
    end

    // Print the output
    always @(posedge clock) begin
        // if(pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.ccm.accum_en_out) begin
        // // //     // $fdisplay(fileID12, "%b", pipeline_0.ccm.interp_data);
        //     $fdisplay(fileID, "%b", pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.ccm.accum_data_out);
        // end
        // $fdisplay(fileID, "In_First: %b, Out_Last: %b", pipeline_wrap_0.gen_pipelines_x[0].gen_pipelines_y[0].pipeline_0.slct.sample_en_in, pipeline_wrap_0.gen_pipelines_x[ACCEL_DIM_X-1].gen_pipelines_y[ACCEL_DIM_Y-1].pipeline_0.ccm.wr_en);
    end // always @(posedge clock)

    initial begin

        $set_gate_level_monitoring("on");
        $set_toggle_region(pipeline_wrap_0);
        $toggle_start;

        // $monitor("Time:%4.0f reset:%b data_in:%d data_out:%d", $time, reset, data_in, data_out);
        ppfile   = $fopen("pipeline.out", "w");
        testfile = $fopen("test.out", "w");
        fileID   = $fopen($psprintf("%s/wrap_random.out", OUTPUT_DATA_FOLDER), "w");

        `ifdef LOOP_SLICES
        for(int slice_idx = 0; slice_idx < Z_GRID_DIM_SIM; slice_idx++) begin
                $display("\n@@\n@@\n@@\nWORKING ON Slice %0d\n@@\n@@\n@@\n", slice_idx);
        `endif
            // Initialize inputs
            clock                       <= `SD 1'b0;
            reset                       <= `SD 1'b0;
            init_mem                    <= `SD 1'b0;

            `ifdef USE_3D
            sample_z_coord_in           <= `SD '0;
            `ifdef USE_3D_CUBE
            tile_z_dim_in               <= `SD '0;
            `else
            z_slice_idx_in              <= `SD Z_SLICE_IDX_SIM;
            `endif
            `endif
            tile_x_dim_in               <= `SD X_TILE_DIM_SIM;
            tile_y_dim_in               <= `SD Y_TILE_DIM_SIM;
            window_dim_in               <= `SD INTERP_WINDOW_DIM_SIM;
            interp_table_consts_in      <= `SD '0;
            interp_table_consts_addr_in <= `SD '0;
            interp_table_consts_en_in   <= `SD '0;
            rd_out_data_addr_in         <= `SD '0;
            rd_out_data_en_in           <= `SD '0;

            sample_data_in              <= `SD '0;
            sample_x_coord_in           <= `SD '0;
            sample_y_coord_in           <= `SD '0;
            sample_en_in                <= `SD '0;

            // Pulse the mem init signal
            $display("@@\n@@\nInitializing Memory......\n@@\n@@\n");
            init_mem                    <= `SD 1'b1;
            @(posedge clock);
            init_mem                    <= `SD 1'b0;

            @(posedge clock);

            // Pulse the reset signal
            $display("@@\n@@\nAsserting System reset......\n@@\n@@\n");
            reset    <= `SD 1'b1;

            @(posedge clock);
            @(posedge clock);
            @(posedge clock);
            
            $display("Reading Table Constants...");
            $fdisplay(ppfile, "Reading Table Constants...");
            interp_table_consts_en_in <= `SD 0; // initialize to zero
            file_table_data_real = $fopen($psprintf("%s/%0d/table_data_real_193.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            file_table_data_imag = $fopen($psprintf("%s/%0d/table_data_imag_193.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            for(int i = 0; i < NUM_TABLE_DATA; i++) begin
                code = $fscanf(file_table_data_real,"%h", table_data[(TABLE_BIT_WIDTH*2)-1:TABLE_BIT_WIDTH]);
                code = $fscanf(file_table_data_imag,"%h", table_data[TABLE_BIT_WIDTH-1:0]);
                // $display("%h", table_data);
                interp_table_consts_in      <= `SD table_data[(TABLE_BIT_WIDTH*2)-1:0]; // take the lower SAMPLE_BIT_WIDTH bits as the value
                interp_table_consts_addr_in <= `SD i;
                interp_table_consts_en_in   <= `SD 1;

                @(posedge clock);
            end // for(int i = 0; i < NUM_CHANNELS_X; i++)
            interp_table_consts_in      <= `SD 0;
            interp_table_consts_addr_in <= `SD 0;
            interp_table_consts_en_in   <= `SD 0;
            $fclose(file_table_data_real); // once reading is finished, close the file
            $fclose(file_table_data_imag); // once reading is finished, close the file
            $display("Done Reading Table Constants.");
            $fdisplay(ppfile, "Done Reading Table Constants.");
            
            @(posedge clock);
            
            $display("@@\n@@\nConstant Initialization Complete...Deasserting System Reset...... %d Cycles\n@@\n@@\n", clock_count);
            reset <= `SD 1'b0;

            @(posedge clock);
            
            $display("Reading input data...");
            $fdisplay(ppfile, "Reading input data...");
            `ifdef USE_3D
            file_input_coord_z = $fopen($psprintf("%s/%0d/input_z_coord_1_%0d.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            `endif
            file_input_coord_x = $fopen($psprintf("%s/%0d/input_x_coord_1_%0d.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            file_input_coord_y = $fopen($psprintf("%s/%0d/input_y_coord_1_%0d.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            file_input_data_real = $fopen($psprintf("%s/%0d/input_data_real_1_%0d.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            file_input_data_imag = $fopen($psprintf("%s/%0d/input_data_imag_1_%0d.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
            while(!$feof(file_input_coord_x)) begin // read line by line until an "end of file" is reached
                // Read the next values
                `ifdef USE_3D
                code = $fscanf(file_input_coord_z,"%h", input_coord_z[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                code = $fscanf(file_input_coord_z,"%h", input_coord_z[(INPUT_BIT_WIDTH/2)-1:0]);
                // $display("%h", input_coord_z);
                // code = $fscanf(file_input_coord_z,"%h", input_coord_z);
                `endif
                code = $fscanf(file_input_coord_x,"%h", input_coord_x[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                code = $fscanf(file_input_coord_x,"%h", input_coord_x[(INPUT_BIT_WIDTH-1)/2:0]);
                // $display("%h", input_coord_x);
                // code = $fscanf(file_input_coord_x,"%h", input_coord_x);
                code = $fscanf(file_input_coord_y,"%h", input_coord_y[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                code = $fscanf(file_input_coord_y,"%h", input_coord_y[(INPUT_BIT_WIDTH/2)-1:0]);
                // $display("%h", input_coord_y);
                // code = $fscanf(file_input_coord_y,"%h", input_coord_y);

                code = $fscanf(file_input_data_real,"%h", input_data_real[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                code = $fscanf(file_input_data_real,"%h", input_data_real[(INPUT_BIT_WIDTH/2)-1:0]);
                // $display("%h", input_data_real);
                // code = $fscanf(file_input_data_real,"%h", input_data_real);
                code = $fscanf(file_input_data_imag,"%h", input_data_imag[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                code = $fscanf(file_input_data_imag,"%h", input_data_imag[(INPUT_BIT_WIDTH/2)-1:0]);
                // $display("%h", input_data_imag);
                // code = $fscanf(file_input_data_imag,"%h", input_data_imag);

                // Assign the values to the signals
                sample_x_coord_in <= `SD input_coord_x;
                sample_y_coord_in <= `SD input_coord_y;
                $fdisplay(ppfile, "Cycles: %d, sample_x_coord_in: %h", clock_count, sample_x_coord_in);
                $fdisplay(ppfile, "Cycles: %d, sample_y_coord_in: %h", clock_count, sample_y_coord_in);
                `ifdef USE_3D
                sample_z_coord_in <= `SD input_coord_z;
                $fdisplay(ppfile, "Cycles: %d, sample_z_coord_in: %h", clock_count, sample_z_coord_in);
                `endif
                sample_data_in <= `SD {input_data_real, input_data_imag};
                // $display("%b", {input_data_real, input_data_imag});
                sample_en_in <= `SD 1;

                @(posedge clock);
            end
            sample_x_coord_in <= `SD 0;
            sample_y_coord_in <= `SD 0;
            sample_data_in <= `SD 0;
            sample_en_in <= `SD 0;
            `ifdef USE_3D
            sample_z_coord_in <= `SD 0;
            $fclose(file_input_coord_z); // once reading is finished, close the file
            `endif
            $fclose(file_input_coord_x); // once reading is finished, close the file
            $fclose(file_input_coord_y); // once reading is finished, close the file
            $fclose(file_input_data_real); // once reading is finished, close the file
            $fclose(file_input_data_imag); // once reading is finished, close the file
            $display("Done reading input data.\n");
            $fdisplay(ppfile, "Done reading input data.");

            for(int i = 0; i <= 20; i++) begin
                @(posedge clock); // extra cycles to account for module propagation delay
            end
        `ifdef LOOP_SLICES
        end
        `endif

        // Finished
        $toggle_stop;
        $toggle_report("PIPELINE_WRAPPER.saif", 1.0e-9, pipeline_wrap_0);

        $display("@@\n@@\nData Stream Complete...Reading Grid Data...... %d Cycles\n@@\n@@\n", clock_count);

        `ifdef USE_3D
        output_file = $fopen($psprintf("%s/final_output_zslice%0d_ALL.out", OUTPUT_DATA_FOLDER, Z_SLICE_IDX_SIM), "w");
        `else
        output_file = $fopen($psprintf("%s/final_output_ALL.out", OUTPUT_DATA_FOLDER), "w");
        `endif
        rd_out_data_en_in <= `SD 1;
        for(int i = 0; i < ACCEL_DIM_X; i++) begin
            for(int j = 0; j < ACCEL_DIM_Y; j++) begin
                `ifdef USE_3D_CUBE
                for(int k = 0; k < ACCEL_DIM_Z; k++) begin
                `endif
                    // output_file = $fopen($psprintf("output_data/final_output_x%0d_y%0d.out", i, j), "w");
                    for(int addr = 0; addr < NUM_BUFFER_DATA; addr++) begin
                        rd_out_data_addr_in <= `SD addr;
                        rd_out_data_en_in <= `SD 1;
                        @(posedge clock);
                        rd_out_data_en_in <= `SD 0;
                        @(posedge clock);
                        @(posedge clock);
                        `ifdef USE_3D_CUBE
                        $fdisplay(output_file, "%b", pipeline_wrap_0.data_out[i][j][k]);
                        `else
                        $fdisplay(output_file, "%b", pipeline_wrap_0.data_out[i][j]);
                        `endif
                    end
                    // $fclose(output_file);
                `ifdef USE_3D_CUBE
                end
                `endif
            end
        end
        rd_out_data_en_in <= `SD 0;
        $fclose(output_file);

        // $display("@@\n@@\nData Stream Complete...... %d Cycles\n@@\n@@\n", clock_count);

        $display("\n@@@Output should be invalid from this point\n\n");

        for(int i = 0; i <= 20; i++) begin
            @(posedge clock); // extra cycles to account for module propagation delay
        end

        $fclose(ppfile);
        $fclose(testfile);
        $display("@@@DONE\n");
        $finish;
    end

endmodule
