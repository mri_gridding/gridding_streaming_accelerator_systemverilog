/////////////////////////////////////////////////////////////////////////
//                                                                     //
//  Module name :  testbench.sv                                        //
//                                                                     //
//  Description :  Testbench for beamforming pipeline; this reads      //
//                 data files and distributes data to various modules. //
//                                                                     //
/////////////////////////////////////////////////////////////////////////

`timescale 1ns/100ps
// // `define LOOP_PIPELINES

module testbench;

    // Parameters
    parameter INPUT_BIT_WIDTH        = 32; // used as a #define for input files
    parameter ACCEL_DIM_X            = 8; // number of pipelines in the x dimension
    parameter ACCEL_DIM_Y            = 8; // number of pipelines in the y dimension
    parameter ACCEL_DIM_Z            = 8; // number of pipelines in the z dimension
    parameter MAX_GRID_DIM_X         = 1024; // max size of the grid in the x dimension
    parameter MAX_GRID_DIM_Y         = 1024; // max size of the grid in the y dimension
    parameter MAX_GRID_DIM_Z         = 1024; // max size of the grid in the z dimension
    parameter TILE_DIM_X             = (MAX_GRID_DIM_X/ACCEL_DIM_X);
    parameter TILE_DIM_Y             = (MAX_GRID_DIM_Y/ACCEL_DIM_Y);
    parameter TILE_DIM_Z             = (MAX_GRID_DIM_Z/ACCEL_DIM_Z);
    parameter MAX_INTERP_WINDOW_SIZE = ACCEL_DIM_X; // max size of the interpolation window in any single dimension
    parameter SAMPLE_BIT_WIDTH       = 32;
    parameter SAMPLE_COORD_BIT_WIDTH = 32;
    parameter TABLE_BIT_WIDTH        = 16;
    parameter TABLE_FRAC_WIDTH       = 14;
    parameter TABLE_OVERSAMP         = 32; // number of table samples between each whole coordinate
    parameter NUM_TABLE_ENTRIES      = (MAX_INTERP_WINDOW_SIZE*TABLE_OVERSAMP);
    `ifdef USE_3D_CUBE
    parameter NUM_BANKS              = 16; // number of banks in each pipeline's data buffer
    parameter NUM_BANK_ENTRIES       = 131072; // number of entries in each data buffer bank
    `else
    parameter NUM_BANKS              = 4; // number of banks in each pipeline's data buffer
    parameter NUM_BANK_ENTRIES       = 4096; // number of entries in each data buffer bank
    `endif
    parameter NUM_BUFFER_ENTRIES     = (NUM_BANKS*NUM_BANK_ENTRIES); // number of entries in each pipeline's data buffer

    `ifdef USE_3D
    localparam INPUT_DATA_FOLDER     = "input_data_3d";
    localparam OUTPUT_DATA_FOLDER    = "output_data_3d";
    `else
    localparam INPUT_DATA_FOLDER     = "input_data";
    localparam OUTPUT_DATA_FOLDER    = "output_data";
    `endif
    localparam NUM_INPUT_POINTS_SIM  = 460800; //  = 460800;
    localparam X_ACCEL_IDX_SIM       = 5;
    localparam Y_ACCEL_IDX_SIM       = 5;
    localparam Z_ACCEL_IDX_SIM       = 0;
    localparam Z_SLICE_IDX_SIM       = 0;
    localparam X_GRID_DIM_SIM        = 768;
    localparam Y_GRID_DIM_SIM        = 768;
    localparam Z_GRID_DIM_SIM        = 768;
    localparam X_TILE_DIM_SIM        = (X_GRID_DIM_SIM/ACCEL_DIM_X);
    localparam Y_TILE_DIM_SIM        = (Y_GRID_DIM_SIM/ACCEL_DIM_Y);
    localparam Z_TILE_DIM_SIM        = (Z_GRID_DIM_SIM/ACCEL_DIM_Z);
    localparam INTERP_WINDOW_DIM_SIM = 6;
    localparam NUM_TABLE_DATA        = 193; // number of table entries in the input file
    `ifdef USE_3D_CUBE
    localparam NUM_BUFFER_DATA       = (X_GRID_DIM_SIM*Y_GRID_DIM_SIM*Z_GRID_DIM_SIM)/(ACCEL_DIM_X*ACCEL_DIM_Y*ACCEL_DIM_Z);
    `else
    localparam NUM_BUFFER_DATA       = (X_GRID_DIM_SIM*Y_GRID_DIM_SIM)/(ACCEL_DIM_X*ACCEL_DIM_Y);
    `endif

    // Testbench Signals
    logic        clock;
    logic        reset;
    logic [31:0] clock_count;

    // Pipeline Signals

    // Inputs
    `ifdef USE_3D
    logic [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_z_coord_in;
    `ifdef USE_3D_CUBE
    logic [$clog2(ACCEL_DIM_Z)-1:0]            accel_z_idx_in;
    logic [$clog2(TILE_DIM_Z)-1:0]             tile_z_dim_in;
    `else
    logic [$clog2(MAX_GRID_DIM_Z)-1:0]         z_slice_idx_in;
    `endif
    `endif
    logic                                      init_mem;
    logic [$clog2(ACCEL_DIM_X)-1:0]            accel_x_idx_in;
    logic [$clog2(ACCEL_DIM_Y)-1:0]            accel_y_idx_in;
    logic [$clog2(TILE_DIM_X)-1:0]             tile_x_dim_in;
    logic [$clog2(TILE_DIM_Y)-1:0]             tile_y_dim_in;
    logic [$clog2(MAX_INTERP_WINDOW_SIZE)-1:0] window_dim_in;
    logic [(TABLE_BIT_WIDTH*2)-1:0]            interp_table_consts_in;
    logic [$clog2(NUM_TABLE_ENTRIES)-1:0]      interp_table_consts_addr_in;
    logic                                      interp_table_consts_en_in;
    logic [$clog2(NUM_BUFFER_ENTRIES)-1:0]     rd_out_data_addr_in; // read/write tile address (can be used for reading final values out)
    logic                                      rd_out_data_en_in; // output a value (for reading final values out)
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]           sample_data_in;
    logic [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_x_coord_in;
    logic [SAMPLE_COORD_BIT_WIDTH-1:0]         sample_y_coord_in;
    logic                                      sample_en_in;

    // Outputs
    logic [(SAMPLE_BIT_WIDTH*2)-1:0]           data_out;
    logic                                      data_en_out;


    // Instantiate the pipeline
    pipeline #(
        `ifdef USE_3D
        .MAX_GRID_DIM_Z(MAX_GRID_DIM_Z),
        `ifdef USE_3D_CUBE
        .ACCEL_DIM_Z(ACCEL_DIM_Z),
        .TILE_DIM_Z(TILE_DIM_Z),
        `endif
        `endif
        .ACCEL_DIM_X(ACCEL_DIM_X),
        .ACCEL_DIM_Y(ACCEL_DIM_Y),
        .MAX_GRID_DIM_X(MAX_GRID_DIM_X),
        .MAX_GRID_DIM_Y(MAX_GRID_DIM_Y),
        .TILE_DIM_X(TILE_DIM_X),
        .TILE_DIM_Y(TILE_DIM_Y),
        .MAX_INTERP_WINDOW_SIZE(MAX_INTERP_WINDOW_SIZE),
        .SAMPLE_BIT_WIDTH(SAMPLE_BIT_WIDTH),
        .SAMPLE_COORD_BIT_WIDTH(SAMPLE_COORD_BIT_WIDTH),
        .TABLE_BIT_WIDTH(TABLE_BIT_WIDTH),
        .TABLE_OVERSAMP(TABLE_OVERSAMP),
        .NUM_TABLE_ENTRIES(NUM_TABLE_ENTRIES),
        .NUM_BANKS(NUM_BANKS),
        .NUM_BANK_ENTRIES(NUM_BANK_ENTRIES),
        .NUM_BUFFER_ENTRIES(NUM_BUFFER_ENTRIES)
        ) pipeline_0 (

        // Inputs
        `ifdef USE_3D
        .sample_z_coord_in(sample_z_coord_in),
        `ifdef USE_3D_CUBE
        .accel_z_idx_in(accel_z_idx_in),
        .tile_z_dim_in(tile_z_dim_in),
        `else
        .z_slice_idx_in(z_slice_idx_in),
        `endif
        `endif
        .clock(clock),
        .reset(reset),
        .init_mem(init_mem),
        .accel_x_idx_in(accel_x_idx_in),
        .accel_y_idx_in(accel_y_idx_in),
        .tile_x_dim_in(tile_x_dim_in),
        .tile_y_dim_in(tile_y_dim_in),
        .window_dim_in(window_dim_in),
        .interp_table_consts_in(interp_table_consts_in),
        .interp_table_consts_addr_in(interp_table_consts_addr_in),
        .interp_table_consts_en_in(interp_table_consts_en_in),
        .rd_out_data_addr_in(rd_out_data_addr_in),
        .rd_out_data_en_in(rd_out_data_en_in),
        .sample_data_in(sample_data_in),
        .sample_x_coord_in(sample_x_coord_in),
        .sample_y_coord_in(sample_y_coord_in),
        .sample_en_in(sample_en_in),

        // Outputs
        .data_out(data_out),
        .data_en_out(data_en_out)

    );

    `ifdef USE_3D
    integer file_input_coord_z; // input file descriptor
    logic [31:0] input_coord_z; // assumes the file always contains 32-bit integers
    integer fileID4a; // output file descriptor
    integer fileID6a; // output file descriptor
    integer fileID8a; // output file descriptor
    integer fileID11a; // output file descriptor
    integer fileID29; // output file descriptor
    integer fileID30; // output file descriptor
    integer fileID31; // output file descriptor
    integer fileID32; // output file descriptor
    integer fileID33; // output file descriptor
    integer fileID34; // output file descriptor
    integer fileID35; // output file descriptor
    integer fileID36; // output file descriptor
    `endif
    integer ppfile; // output file descriptor
    integer testfile; // output file descriptor
    integer output_file; // output file descriptor
    integer fileID1; // output file descriptor
    integer fileID2; // output file descriptor
    integer fileID3; // output file descriptor
    integer fileID4; // output file descriptor
    integer fileID5; // output file descriptor
    integer fileID6; // output file descriptor
    integer fileID7; // output file descriptor
    integer fileID8; // output file descriptor
    integer fileID9; // output file descriptor
    integer fileID10; // output file descriptor
    integer fileID11; // output file descriptor
    integer fileID12; // output file descriptor
    integer fileID13; // output file descriptor
    integer fileID14; // output file descriptor
    integer fileID15; // output file descriptor
    integer fileID16; // output file descriptor
    integer fileID17; // output file descriptor
    integer fileID18; // output file descriptor
    integer fileID19; // output file descriptor
    integer fileID20; // output file descriptor
    integer fileID21; // output file descriptor
    integer fileID22; // output file descriptor
    integer fileID23; // output file descriptor
    integer fileID24; // output file descriptor
    integer fileID25; // output file descriptor
    integer fileID26; // output file descriptor
    integer fileID27; // output file descriptor
    integer fileID28; // output file descriptor
    integer file_input_coord_x; // input file descriptor
    integer file_input_coord_y; // input file descriptor
    integer file_input_data; // input file descriptor
    integer file_input_data_real; // input file descriptor
    integer file_input_data_imag; // input file descriptor
    integer file_table_data_real; // input file descriptor
    integer file_table_data_imag; // input file descriptor
    integer code; // return code for fscanf
    logic [63:0] input_data; // assumes the file always contains 32-bit integers
    logic [31:0] input_data_real; // assumes the file always contains 32-bit integers
    logic [31:0] input_data_imag; // assumes the file always contains 32-bit integers
    logic [31:0] input_coord_x; // assumes the file always contains 32-bit integers
    logic [31:0] input_coord_y; // assumes the file always contains 32-bit integers
    logic [31:0] table_data; // assumes the file always contains 32-bit integers

    // Generate System Clock
    always begin
        #(`VERILOG_CLOCK_PERIOD/2.0);
        clock = ~clock;
    end

    // Count the number of posedges and number of instructions completed
    // till simulation ends
    always @(posedge clock) begin
        if(reset) begin
            clock_count <= `SD 0;
            // instr_count <= `SD 0;
        end else begin
            clock_count <= `SD (clock_count + 1);
            // instr_count <= `SD (instr_count + pipeline_completed_insts);
        end
    end

    // Print the output
    always @(posedge clock) begin
        if(pipeline_0.slct.sample_en_in) begin
            $fdisplay(fileID1, "%b", pipeline_0.slct.x_base_tile_idx_next);
            $fdisplay(fileID2, "%b", pipeline_0.slct.y_base_tile_idx_next);
            $fdisplay(fileID3, "%b", pipeline_0.slct.x_coord_next);
            $fdisplay(fileID4, "%b", pipeline_0.slct.y_coord_next);
            `ifdef USE_3D
            $fdisplay(fileID4a, "%b", pipeline_0.slct.z_coord_next);
            `endif
        end
        if(pipeline_0.slct.sample_en_saved) begin
            $fdisplay(fileID5, "%b", pipeline_0.slct.x_distance_next);
            $fdisplay(fileID6, "%b", pipeline_0.slct.y_distance_next);
            `ifdef USE_3D
            $fdisplay(fileID6a, "%b", pipeline_0.slct.z_distance_next);
            `endif
        end
        if(pipeline_0.slct.sample_en_saved_2 & pipeline_0.slct.pipeline_affected_next) begin
            $fdisplay(fileID7, "%b", pipeline_0.slct.x_tile_idx);
            $fdisplay(fileID8, "%b", pipeline_0.slct.y_tile_idx);

            $fdisplay(fileID9, "%b", pipeline_0.slct.xy_tile_idx_next);
            $fdisplay(fileID10, "%b", pipeline_0.slct.x_table_idx_next);
            $fdisplay(fileID11, "%b", pipeline_0.slct.y_table_idx_next);
            `ifdef USE_3D
            $fdisplay(fileID11a, "%b", pipeline_0.slct.z_table_idx_next);
            `endif
        end
        // if(pipeline_0.slct.x_affected_next) begin
        //     $fdisplay(fileID12, "x");
        // end
        // if(pipeline_0.slct.y_affected_next) begin
        //     $fdisplay(fileID12, "y");
        // end



        if(pipeline_0.ntrp_tbl.select_en_in) begin
            $fdisplay(testfile, "%b", pipeline_0.ntrp_tbl.select_addr_x_in);
            $fdisplay(testfile, "%b", pipeline_0.ntrp_tbl.select_addr_y_in);
            // $fdisplay(testfile, "rd_x_data_next: %b", pipeline_0.ntrp_tbl.addr_x_in);
            // $fdisplay(testfile, "rd_y_data_next: %b", pipeline_0.ntrp_tbl.addr_y_in);
        end
        if(pipeline_0.ntrp_tbl.k3_val_en) begin
            $fdisplay(fileID13, "%b", pipeline_0.ntrp_tbl.k1_next);
            $fdisplay(fileID14, "%b", pipeline_0.ntrp_tbl.k2_next);
            $fdisplay(fileID15, "%b", pipeline_0.ntrp_tbl.k3_next);
        end
        if(pipeline_0.ntrp_tbl.k6_val_en) begin
            $fdisplay(fileID16, "%b", pipeline_0.ntrp_tbl.k4_next);
            $fdisplay(fileID17, "%b", pipeline_0.ntrp_tbl.k5_next);
            $fdisplay(fileID18, "%b", pipeline_0.ntrp_tbl.k6_next);
        end
        if(pipeline_0.ntrp_tbl.i1_val_en) begin
            $fdisplay(fileID19, "%b", pipeline_0.ntrp_tbl.r1_next);
            $fdisplay(fileID20, "%b", pipeline_0.ntrp_tbl.i1_next);
        end
        `ifdef USE_3D
        if(pipeline_0.ntrp_tbl.k9_val_en) begin
            $fdisplay(fileID29, "%b", pipeline_0.ntrp_tbl.k7_next);
            $fdisplay(fileID30, "%b", pipeline_0.ntrp_tbl.k8_next);
            $fdisplay(fileID31, "%b", pipeline_0.ntrp_tbl.k9_next);
        end
        if(pipeline_0.ntrp_tbl.k12_val_en) begin
            $fdisplay(fileID32, "%b", pipeline_0.ntrp_tbl.k10_next);
            $fdisplay(fileID33, "%b", pipeline_0.ntrp_tbl.k11_next);
            $fdisplay(fileID34, "%b", pipeline_0.ntrp_tbl.k12_next);
        end
        if(pipeline_0.ntrp_tbl.i2_val_en) begin
            $fdisplay(fileID35, "%b", pipeline_0.ntrp_tbl.r2_next);
            $fdisplay(fileID36, "%b", pipeline_0.ntrp_tbl.i2_next);
        end
        `endif


        if(pipeline_0.ntrp.interp_table_en_in) begin
            $fdisplay(fileID21, "%b", pipeline_0.ntrp.k7_next);
            $fdisplay(fileID22, "%b", pipeline_0.ntrp.k8_next);
            $fdisplay(fileID23, "%b", pipeline_0.ntrp.k9_next);
        end
        if(pipeline_0.ntrp.k9_val_en) begin
            $fdisplay(fileID24, "%b", pipeline_0.ntrp.k10_next);
            $fdisplay(fileID25, "%b", pipeline_0.ntrp.k11_next);
            $fdisplay(fileID26, "%b", pipeline_0.ntrp.k12_next);

            // $fdisplay(fileID12, "%b", pipeline_0.ntrp.sample_real);
            // $fdisplay(fileID12, "%b", pipeline_0.ntrp.k7);
        end
        if(pipeline_0.ntrp.k12_val_en) begin
            $fdisplay(fileID27, "%b", pipeline_0.ntrp.r2_next);
            $fdisplay(fileID28, "%b", pipeline_0.ntrp.i2_next);
        end
        // if(pipeline_0.ntrp.interp_en_out) begin
        //     $fdisplay(fileID12, "%b", pipeline_0.ntrp.interp_table_real_out);
        //     $fdisplay(fileID12, "%b", pipeline_0.ntrp.interp_table_imag_out);
        //     // $fdisplay(fileID12, "%b", pipeline_0.select_en_out_5);
        // end
        // if(pipeline_0.interp_en_out) begin
        //     $fdisplay(fileID12, "%b", pipeline_0.interp_data_out);
        // end
        // if(pipeline_0.select_en_out) begin
        //     $fdisplay(fileID12, "%b", pipeline_0.tile_idx_out);
        // end
        // if(pipeline_0.ccm.interp_en_in) begin
        //     $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.interp_tile_idx_in, clock_count);
        //     $fdisplay(fileID12, "%b", pipeline_0.ccm.interp_data_in);
        // end
        // if(pipeline_0.ccm.accum_en_out) begin
        //     $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.accum_data_out, clock_count);
        // end
        // if(pipeline_0.ccm.accum_rd_en_in) begin
        //     $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.interp_tile_idx_in, clock_count);
        // end
        // $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.interp_tile_idx_in, clock_count);
        // $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.interp_data_in, clock_count);
        // $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.interp_en_in, clock_count);
        // $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.accum_rd_en_in, clock_count);
        // if(pipeline_0.ccm.interp_en_in) begin
        //     // $fdisplay(fileID12, "%b", pipeline_0.ccm.interp_tile_idx_in);
        //     $fdisplay(fileID12, "%b%b", pipeline_0.ccm.bank_rd_idx, pipeline_0.ccm.bank_rd_addr[pipeline_0.ccm.bank_rd_idx]);
        // end
        // if(pipeline_0.ccm.wr_en) begin
        //     // $fdisplay(fileID12, "%b", pipeline_0.ccm.wr_addr);
        //     // $fdisplay(fileID12, "%b", pipeline_0.ccm.bank_wr_idx);
        //     // $fdisplay(fileID12, "%b", pipeline_0.ccm.bank_wr_addr[pipeline_0.ccm.bank_wr_idx]);
        //     // $fdisplay(fileID12, "%b", pipeline_0.ccm.bank_wr_en);
        //     // $fdisplay(fileID12, "%b%b%b", pipeline_0.ccm.bank_wr_idx, pipeline_0.ccm.bank_wr_addr[pipeline_0.ccm.bank_wr_idx], pipeline_0.ccm.bank_wr_data[pipeline_0.ccm.bank_wr_idx]);
        //     // $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.bank_wr_en, clock_count);
        //     // $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.bank_wr_idx, clock_count);
        //     // $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.bank_wr_addr[pipeline_0.ccm.bank_wr_idx], clock_count);
        //     $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.bank_wr_data[pipeline_0.ccm.bank_wr_idx], clock_count);
        // end
        // if(pipeline_0.ccm.interp_en) begin
        //     $fdisplay(fileID12, "%b_%0d", pipeline_0.ccm.bank_rd_data_next[pipeline_0.ccm.bank_rd_idx_saved], clock_count);
        // end
        // if(pipeline_0.ccm.interp_en && (pipeline_0.ccm.interp_addr == pipeline_0.ccm.wr_addr)) begin
        //     $fdisplay(fileID12, "%0d", clock_count);
        // end
    end // always @(posedge clock)

    initial begin

        $set_gate_level_monitoring("on");
        $set_toggle_region(pipeline_0);
        $toggle_start;

        // $monitor("Time:%4.0f reset:%b data_in:%d data_out:%d", $time, reset, data_in, data_out);
        ppfile = $fopen("pipeline.out", "w");
        testfile = $fopen("test.out", "w");

        `ifdef USE_3D
        fileID4a = $fopen($psprintf("%s/z_coord.out", OUTPUT_DATA_FOLDER), "w");
        fileID6a = $fopen($psprintf("%s/z_distance.out", OUTPUT_DATA_FOLDER), "w");
        fileID8a = $fopen($psprintf("%s/z_tile_idx.out", OUTPUT_DATA_FOLDER), "w");
        fileID11a = $fopen($psprintf("%s/z_table_idx.out", OUTPUT_DATA_FOLDER), "w");
        fileID29 = $fopen($psprintf("%s/k7.out", OUTPUT_DATA_FOLDER), "w");
        fileID30 = $fopen($psprintf("%s/k8.out", OUTPUT_DATA_FOLDER), "w");
        fileID31 = $fopen($psprintf("%s/k9.out", OUTPUT_DATA_FOLDER), "w");
        fileID32 = $fopen($psprintf("%s/k10.out", OUTPUT_DATA_FOLDER), "w");
        fileID33 = $fopen($psprintf("%s/k11.out", OUTPUT_DATA_FOLDER), "w");
        fileID34 = $fopen($psprintf("%s/k12.out", OUTPUT_DATA_FOLDER), "w");
        fileID35 = $fopen($psprintf("%s/r2.out", OUTPUT_DATA_FOLDER), "w");
        fileID36 = $fopen($psprintf("%s/i2.out", OUTPUT_DATA_FOLDER), "w");
        fileID21 = $fopen($psprintf("%s/k13.out", OUTPUT_DATA_FOLDER), "w");
        fileID22 = $fopen($psprintf("%s/k14.out", OUTPUT_DATA_FOLDER), "w");
        fileID23 = $fopen($psprintf("%s/k15.out", OUTPUT_DATA_FOLDER), "w");
        fileID24 = $fopen($psprintf("%s/k16.out", OUTPUT_DATA_FOLDER), "w");
        fileID25 = $fopen($psprintf("%s/k17.out", OUTPUT_DATA_FOLDER), "w");
        fileID26 = $fopen($psprintf("%s/k18.out", OUTPUT_DATA_FOLDER), "w");
        fileID27 = $fopen($psprintf("%s/R3.out", OUTPUT_DATA_FOLDER), "w");
        fileID28 = $fopen($psprintf("%s/I3.out", OUTPUT_DATA_FOLDER), "w");
        `else
        fileID21 = $fopen($psprintf("%s/k7.out", OUTPUT_DATA_FOLDER), "w");
        fileID22 = $fopen($psprintf("%s/k8.out", OUTPUT_DATA_FOLDER), "w");
        fileID23 = $fopen($psprintf("%s/k9.out", OUTPUT_DATA_FOLDER), "w");
        fileID24 = $fopen($psprintf("%s/k10.out", OUTPUT_DATA_FOLDER), "w");
        fileID25 = $fopen($psprintf("%s/k11.out", OUTPUT_DATA_FOLDER), "w");
        fileID26 = $fopen($psprintf("%s/k12.out", OUTPUT_DATA_FOLDER), "w");
        fileID27 = $fopen($psprintf("%s/r2.out", OUTPUT_DATA_FOLDER), "w");
        fileID28 = $fopen($psprintf("%s/i2.out", OUTPUT_DATA_FOLDER), "w");
        `endif
        fileID1 = $fopen($psprintf("%s/x_base_tile_idx.out", OUTPUT_DATA_FOLDER), "w");
        fileID2 = $fopen($psprintf("%s/y_base_tile_idx.out", OUTPUT_DATA_FOLDER), "w");
        fileID3 = $fopen($psprintf("%s/x_coord.out", OUTPUT_DATA_FOLDER), "w");
        fileID4 = $fopen($psprintf("%s/y_coord.out", OUTPUT_DATA_FOLDER), "w");
        fileID5 = $fopen($psprintf("%s/x_distance.out", OUTPUT_DATA_FOLDER), "w");
        fileID6 = $fopen($psprintf("%s/y_distance.out", OUTPUT_DATA_FOLDER), "w");
        fileID7 = $fopen($psprintf("%s/x_tile_idx.out", OUTPUT_DATA_FOLDER), "w");
        fileID8 = $fopen($psprintf("%s/y_tile_idx.out", OUTPUT_DATA_FOLDER), "w");
        fileID9 = $fopen($psprintf("%s/xy_tile_idx.out", OUTPUT_DATA_FOLDER), "w");
        fileID10 = $fopen($psprintf("%s/x_table_idx.out", OUTPUT_DATA_FOLDER), "w");
        fileID11 = $fopen($psprintf("%s/y_table_idx.out", OUTPUT_DATA_FOLDER), "w");
        fileID12 = $fopen($psprintf("%s/random.out", OUTPUT_DATA_FOLDER), "w");
        fileID13 = $fopen($psprintf("%s/k1.out", OUTPUT_DATA_FOLDER), "w");
        fileID14 = $fopen($psprintf("%s/k2.out", OUTPUT_DATA_FOLDER), "w");
        fileID15 = $fopen($psprintf("%s/k3.out", OUTPUT_DATA_FOLDER), "w");
        fileID16 = $fopen($psprintf("%s/k4.out", OUTPUT_DATA_FOLDER), "w");
        fileID17 = $fopen($psprintf("%s/k5.out", OUTPUT_DATA_FOLDER), "w");
        fileID18 = $fopen($psprintf("%s/k6.out", OUTPUT_DATA_FOLDER), "w");
        fileID19 = $fopen($psprintf("%s/r1.out", OUTPUT_DATA_FOLDER), "w");
        fileID20 = $fopen($psprintf("%s/i1.out", OUTPUT_DATA_FOLDER), "w");

        `ifdef LOOP_PIPELINES
        for(int accel_x_idx = 0; accel_x_idx < ACCEL_DIM_X; accel_x_idx++) begin
            for(int accel_y_idx = 0; accel_y_idx < ACCEL_DIM_Y; accel_y_idx++) begin
                `ifdef USE_3D
                `ifdef USE_3D_CUBE
                for(int accel_z_idx = 0; accel_z_idx < ACCEL_DIM_Z; accel_z_idx++) begin
                $display("\n@@\n@@\n@@\nWORKING ON PIPELINE (%0d, %0d, %0d)\n@@\n@@\n@@\n", accel_x_idx, accel_y_idx, accel_z_idx);
                `else
                for(int z_slice_idx = 0; z_slice_idx < Z_GRID_DIM_SIM; z_slice_idx++) begin
                $display("\n@@\n@@\n@@\nWORKING ON SLICE %0d, PIPELINE (%0d, %0d)\n@@\n@@\n@@\n", z_slice_idx, accel_x_idx, accel_y_idx);
                `endif
                `else
                $display("\n@@\n@@\n@@\nWORKING ON PIPELINE (%0d, %0d)\n@@\n@@\n@@\n", accel_x_idx, accel_y_idx);
                `endif
        `endif
                // Initialize inputs
                clock                       <= `SD 1'b0;
                reset                       <= `SD 1'b0;
                init_mem                    <= `SD 1'b0;

                `ifdef USE_3D // if USE_3D
                sample_z_coord_in           <= `SD '0;
                `ifdef USE_3D_CUBE // if USE_3D *AND* USE_3D_CUBE
                tile_z_dim_in               <= `SD Z_TILE_DIM_SIM;
                `ifdef LOOP_PIPELINES // if USE_3D *AND* USE_3D_CUBE *AND* LOOP_PIPELINES
                accel_z_idx_in              <= `SD accel_z_idx;
                `else // if USE_3D *AND* USE_3D_CUBE *AND NOT* LOOP_PIPELINES
                accel_z_idx_in              <= `SD Z_ACCEL_IDX_SIM;
                `endif
                `else
                `ifdef LOOP_PIPELINES // if USE_3D *AND NOT* USE_3D_CUBE *AND* LOOP_PIPELINES
                z_slice_idx_in              <= `SD z_slice_idx;
                `else // if USE_3D *AND NOT* USE_3D_CUBE *AND NOT* LOOP_PIPELINES
                z_slice_idx_in              <= `SD Z_SLICE_IDX_SIM;
                `endif
                `endif
                `endif

                `ifdef LOOP_PIPELINES
                accel_x_idx_in              <= `SD accel_x_idx;
                accel_y_idx_in              <= `SD accel_y_idx;
                `else
                accel_x_idx_in              <= `SD X_ACCEL_IDX_SIM;
                accel_y_idx_in              <= `SD Y_ACCEL_IDX_SIM;
                `endif
                tile_x_dim_in               <= `SD X_TILE_DIM_SIM;
                tile_y_dim_in               <= `SD Y_TILE_DIM_SIM;
                window_dim_in               <= `SD INTERP_WINDOW_DIM_SIM;
                interp_table_consts_in      <= `SD '0;
                interp_table_consts_addr_in <= `SD '0;
                interp_table_consts_en_in   <= `SD '0;
                rd_out_data_addr_in         <= `SD '0;
                rd_out_data_en_in           <= `SD '0;

                sample_data_in              <= `SD '0;
                sample_x_coord_in           <= `SD '0;
                sample_y_coord_in           <= `SD '0;
                sample_en_in                <= `SD '0;

                // Pulse the mem init signal
                $display("@@\n@@\nInitializing Memory......\n@@\n@@\n");
                init_mem                    <= `SD 1'b1;
                @(posedge clock);
                init_mem                    <= `SD 1'b0;

                @(posedge clock);

                // Pulse the reset signal
                $display("@@\n@@\nAsserting System reset......\n@@\n@@\n");
                reset    <= `SD 1'b1;

                @(posedge clock);
                @(posedge clock);
                @(posedge clock);
                
                $display("Reading Table Constants...");
                $fdisplay(ppfile, "Reading Table Constants...");
                interp_table_consts_en_in <= `SD 0; // initialize to zero
                file_table_data_real = $fopen($psprintf("%s/%0d/table_data_real_193.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
                file_table_data_imag = $fopen($psprintf("%s/%0d/table_data_imag_193.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
                for(int i = 0; i < NUM_TABLE_DATA; i++) begin
                    code = $fscanf(file_table_data_real,"%h", table_data[(TABLE_BIT_WIDTH*2)-1:TABLE_BIT_WIDTH]);
                    code = $fscanf(file_table_data_imag,"%h", table_data[TABLE_BIT_WIDTH-1:0]);
                    // $display("%h", table_data);
                    interp_table_consts_in      <= `SD table_data[(TABLE_BIT_WIDTH*2)-1:0]; // take the lower SAMPLE_BIT_WIDTH bits as the value
                    interp_table_consts_addr_in <= `SD i;
                    interp_table_consts_en_in   <= `SD 1;

                    @(posedge clock);
                end // for(int i = 0; i < NUM_CHANNELS_X; i++)
                interp_table_consts_in      <= `SD 0;
                interp_table_consts_addr_in <= `SD 0;
                interp_table_consts_en_in   <= `SD 0;
                $fclose(file_table_data_real); // once reading is finished, close the file
                $fclose(file_table_data_imag); // once reading is finished, close the file
                $display("Done Reading Table Constants.");
                $fdisplay(ppfile, "Done Reading Table Constants.");
                
                @(posedge clock);
                
                $display("@@\n@@\nConstant Initialization Complete...Deasserting System Reset...... %d Cycles\n@@\n@@\n", clock_count);
                reset <= `SD 1'b0;

                @(posedge clock);
                
                $display("Reading input data...");
                $fdisplay(ppfile, "Reading input data...");
                `ifdef USE_3D
                file_input_coord_z = $fopen($psprintf("%s/%0d/input_z_coord_1_%0d.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
                `endif
                file_input_coord_x = $fopen($psprintf("%s/%0d/input_x_coord_1_%0d.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
                file_input_coord_y = $fopen($psprintf("%s/%0d/input_y_coord_1_%0d.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
                file_input_data_real = $fopen($psprintf("%s/%0d/input_data_real_1_%0d.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
                file_input_data_imag = $fopen($psprintf("%s/%0d/input_data_imag_1_%0d.data", INPUT_DATA_FOLDER, NUM_INPUT_POINTS_SIM, NUM_INPUT_POINTS_SIM), "rb"); // "rb" means reading binary
                while(!$feof(file_input_coord_x)) begin // read line by line until an "end of file" is reached
                    // Read the next values
                    `ifdef USE_3D
                    code = $fscanf(file_input_coord_z,"%h", input_coord_z[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                    code = $fscanf(file_input_coord_z,"%h", input_coord_z[(INPUT_BIT_WIDTH/2)-1:0]);
                    // $display("%h", input_coord_z);
                    // code = $fscanf(file_input_coord_z,"%h", input_coord_z);
                    `endif
                    code = $fscanf(file_input_coord_x,"%h", input_coord_x[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                    code = $fscanf(file_input_coord_x,"%h", input_coord_x[(INPUT_BIT_WIDTH-1)/2:0]);
                    // $display("%h", input_coord_x);
                    // code = $fscanf(file_input_coord_x,"%h", input_coord_x);
                    code = $fscanf(file_input_coord_y,"%h", input_coord_y[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                    code = $fscanf(file_input_coord_y,"%h", input_coord_y[(INPUT_BIT_WIDTH/2)-1:0]);
                    // $display("%h", input_coord_y);
                    // code = $fscanf(file_input_coord_y,"%h", input_coord_y);

                    code = $fscanf(file_input_data_real,"%h", input_data_real[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                    code = $fscanf(file_input_data_real,"%h", input_data_real[(INPUT_BIT_WIDTH/2)-1:0]);
                    // $display("%h", input_data_real);
                    // code = $fscanf(file_input_data_real,"%h", input_data_real);
                    code = $fscanf(file_input_data_imag,"%h", input_data_imag[INPUT_BIT_WIDTH-1:INPUT_BIT_WIDTH/2]);
                    code = $fscanf(file_input_data_imag,"%h", input_data_imag[(INPUT_BIT_WIDTH/2)-1:0]);
                    // $display("%h", input_data_imag);
                    // code = $fscanf(file_input_data_imag,"%h", input_data_imag);

                    // Assign the values to the signals
                    sample_x_coord_in <= `SD input_coord_x;
                    sample_y_coord_in <= `SD input_coord_y;
                    $fdisplay(ppfile, "Cycles: %d, sample_x_coord_in: %h", clock_count, sample_x_coord_in);
                    $fdisplay(ppfile, "Cycles: %d, sample_y_coord_in: %h", clock_count, sample_y_coord_in);
                    `ifdef USE_3D
                    sample_z_coord_in <= `SD input_coord_z;
                    $fdisplay(ppfile, "Cycles: %d, sample_z_coord_in: %h", clock_count, sample_z_coord_in);
                    `endif
                    sample_data_in <= `SD {input_data_real, input_data_imag};
                    // $display("%b", {input_data_real, input_data_imag});
                    sample_en_in <= `SD 1;

                    @(posedge clock);
                end
                sample_x_coord_in <= `SD 0;
                sample_y_coord_in <= `SD 0;
                sample_data_in <= `SD 0;
                sample_en_in <= `SD 0;
                `ifdef USE_3D
                sample_z_coord_in <= `SD 0;
                $fclose(file_input_coord_z); // once reading is finished, close the file
                `endif
                $fclose(file_input_coord_x); // once reading is finished, close the file
                $fclose(file_input_coord_y); // once reading is finished, close the file
                $fclose(file_input_data_real); // once reading is finished, close the file
                $fclose(file_input_data_imag); // once reading is finished, close the file
                $display("Done reading input data.\n");
                $fdisplay(ppfile, "Done reading input data.");
        `ifdef LOOP_PIPELINES
                `ifdef USE_3D
                end
                `endif
            end
        end
        `endif

        for(int i = 0; i <= 20; i++) begin
            @(posedge clock); // extra cycles to account for module propagation delay
        end

        // Finished
        $toggle_stop;
        $toggle_report("PIPELINE.saif", 1.0e-9, pipeline_0);

        $display("@@\n@@\nData Stream Complete...Reading Grid Data...... %d Cycles\n@@\n@@\n", clock_count);

        `ifdef USE_3D
        output_file = $fopen($psprintf("%s/final_output_zslice%0d_x%0d_y%0d.out", OUTPUT_DATA_FOLDER, Z_SLICE_IDX_SIM, X_ACCEL_IDX_SIM, Y_ACCEL_IDX_SIM), "w");
        `else
        output_file = $fopen($psprintf("%s/final_output_x%0d_y%0d.out", OUTPUT_DATA_FOLDER, X_ACCEL_IDX_SIM, Y_ACCEL_IDX_SIM), "w");
        `endif
        rd_out_data_en_in <= `SD 1;
        for(int i = 0; i < NUM_BUFFER_DATA; i++) begin
            rd_out_data_addr_in <= `SD i;
            rd_out_data_en_in <= `SD 1;
            @(posedge clock);
            rd_out_data_en_in <= `SD 0;
            @(posedge clock);
            @(posedge clock);
            $fdisplay(output_file, "%b", pipeline_0.data_out);
        end
        rd_out_data_en_in <= `SD 0;
        $fclose(output_file);

        // $display("@@\n@@\nData Stream Complete...... %d Cycles\n@@\n@@\n", clock_count);

        $display("\n@@@Output should be invalid from this point\n\n");

        for(int i = 0; i <= 20; i++) begin
            @(posedge clock); // extra cycles to account for module propagation delay
        end

        `ifdef USE_3D
        $fclose(fileID4a);
        $fclose(fileID6a);
        $fclose(fileID8a);
        $fclose(fileID11a);
        $fclose(fileID29);
        $fclose(fileID30);
        $fclose(fileID31);
        $fclose(fileID32);
        $fclose(fileID33);
        $fclose(fileID34);
        $fclose(fileID35);
        $fclose(fileID36);
        `endif
        $fclose(fileID1);
        $fclose(fileID2);
        $fclose(fileID3);
        $fclose(fileID4);
        $fclose(fileID5);
        $fclose(fileID6);
        $fclose(fileID7);
        $fclose(fileID8);
        $fclose(fileID9);
        $fclose(fileID10);
        $fclose(fileID11);
        $fclose(fileID12);
        $fclose(fileID13);
        $fclose(fileID14);
        $fclose(fileID15);
        $fclose(fileID16);
        $fclose(fileID17);
        $fclose(fileID18);
        $fclose(fileID19);
        $fclose(fileID20);
        $fclose(fileID21);
        $fclose(fileID22);
        $fclose(fileID23);
        $fclose(fileID24);
        $fclose(fileID25);
        $fclose(fileID26);
        $fclose(fileID27);
        $fclose(fileID28);

        $fclose(ppfile);
        $fclose(testfile);
        $display("@@@DONE\n");
        $finish;
    end

endmodule
