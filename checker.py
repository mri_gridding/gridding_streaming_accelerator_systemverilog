#!/usr/bin/env python
import filecmp
import os
import fileinput

def prRed(skk): print("\033[91m {}\033[00m" .format(skk))
def prGreen(skk): print("\033[92m {}\033[00m" .format(skk))
def prYellow(skk): print("\033[93m {}\033[00m" .format(skk))
def prLightPurple(skk): print("\033[94m {}\033[00m" .format(skk))
def prPurple(skk): print("\033[95m {}\033[00m" .format(skk))
def prCyan(skk): print("\033[96m {}\033[00m" .format(skk))
def prLightGray(skk): print("\033[97m {}\033[00m" .format(skk))
def prBlack(skk): print("\033[98m {}\033[00m" .format(skk))


# data_folder = "output_data"
data_folder = "output_data_3d"

# num_inputs = "460800"
# num_inputs = "200000"
num_inputs = "50000"
# num_inputs = "5000"
# num_inputs = "1000"

# input_file = "final_output_ALL.out"
# input_file = "final_output_x5_y5.out"
input_file = "final_output_zslice0_x5_y5.out"
# input_file = "random.out"
# input_file = "x_base_tile_idx.out"
# input_file = "y_base_tile_idx.out"
# input_file = "z_base_tile_idx.out"
# input_file = "x_coord.out"
# input_file = "y_coord.out"
# input_file = "z_coord.out"
# input_file = "x_distance.out"
# input_file = "x_tile_idx.out"
# input_file = "y_tile_idx.out"
# input_file = "xy_tile_idx.out"
# input_file = "x_table_idx.out"
# input_file = "y_table_idx.out"
# input_file = "z_table_idx.out"
# input_file = "k1.out"
# input_file = "k2.out"
# input_file = "k3.out"
# input_file = "k4.out"
# input_file = "k5.out"
# input_file = "k6.out"
# input_file = "R1.out"
# input_file = "I1.out"
# input_file = "k7.out"
# input_file = "k8.out"
# input_file = "k9.out"
# input_file = "k10.out"
# input_file = "k11.out"
# input_file = "k12.out"
# input_file = "R2.out"
# input_file = "I2.out"
# input_file = "k13.out"
# input_file = "k14.out"
# input_file = "k15.out"
# input_file = "k16.out"
# input_file = "k17.out"
# input_file = "k18.out"
# input_file = "R3.out"
# input_file = "I3.out"

os.system('cat %s/%s | tr -d "[:blank:]" > %s/checker_files/test.out.check' % (data_folder, input_file, data_folder))

if filecmp.cmp('%s/checker_files/test.out.check' % (data_folder), '%s/checker_files/%s/%s' % (data_folder, num_inputs, input_file)):
	prGreen("Pass :D")
else:
	prRed("Fail :(")

os.remove('%s/checker_files/test.out.check' % (data_folder))
